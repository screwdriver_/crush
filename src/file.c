#include "crush.h"

#define FILE_PREFIX		"/tmp/crush_"
#define FILE_PREFIX_LEN	11
#define RANDOM_LENGTH	8
#define MAX_ATTEMPTS	32

// TODO Make cross-platform
static const char *rand_path(void)
{
	char *path;
	char buffer[RANDOM_LENGTH];
	int fd;
	size_t i;

	if(!(path = mem_alloc(FILE_PREFIX_LEN + RANDOM_LENGTH + 1)))
		return NULL;
	strcpy(path, FILE_PREFIX);

	if((fd = c_open("/dev/urandom", O_RDONLY)) < 0)
		return NULL;
	bzero(buffer, sizeof(buffer));
	read(fd, buffer, sizeof(buffer));
	for(i = 0; i < sizeof(buffer); ++i)
		buffer[i] = 'a' + (buffer[i] * 26 / 255);
	close(fd);

	memcpy(path + FILE_PREFIX_LEN, buffer, RANDOM_LENGTH);
	return path;
}

// TODO Race condition
const char *new_tmp_file(void)
{
	const char *path;
	size_t attempts = 0;

	do
	{
		if(access((path = rand_path()), F_OK) >= 0)
		{
			free((void *) path);
			path = NULL;
		}
	}
	while(!path && attempts++ < MAX_ATTEMPTS);
	return path;
}

char *read_file(const char *file)
{
	int fd;
	size_t len;
	char *content;

	if(!file || (fd = c_open(file, O_RDONLY)) < 0)
		return NULL;
	len = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	if(!(content = mem_alloc(len + 1)))
		return NULL;
	c_read(fd, content, len);
	return content;
}

char *read_file_fd(const int fd)
{
	size_t len;
	char *content;

	len = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	if(!(content = mem_alloc(len + 1)))
		return NULL;
	c_read(fd, content, len);
	return content;
}
