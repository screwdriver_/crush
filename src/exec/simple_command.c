#include "exec.h"

__attribute__((noreturn))
static void exec_cmd(ast_t *ast, command_context_t *cmd_context)
{
	if(redirect(ast) < 0)
		exit(1);
	exit(execve(cmd_context->bin_path, (char *const *) cmd_context->argv,
		(char *const *) env_array(cmd_context->environ)));
}

// TODO Builtin must fork if variable assignement
void e_simple_command(ast_t *ast, exec_context_t *context)
{
	command_context_t cmd_context;
	process_t *proc = NULL;
	pid_t pid;

	if(prepare_context(ast, &cmd_context) < 0)
	{
		exit_code = 1;
		goto end;
	}
	if(!cmd_context.argv || !cmd_context.argv[0])
	{
		exit_code = 0;
		goto end;
	}
	if(cmd_context.builtin) // TODO Builtin redirects
	{
		exit_code = cmd_context.builtin->func(context, &cmd_context);
		goto end;
	}
	if(!cmd_context.bin_path)
	{
		command_not_found(cmd_context.argv[0]);
		exit_code = 127;
		goto end;
	}

	if((pid = c_fork()) < 0)
	{
		exit_code = 1;
		goto end;
	}
	if(pid)
	{
		if(!(proc = process_new(cmd_context.argv, pid)))
			goto end;
		if(context->job)
		{
			job_add_process(context->job, proc);
			(context->bg ? job_background : job_foreground)(context->job, 0);
		}
		else
		{
			wait_process(proc);
			process_remove(proc);
		}
		goto end;
	}
	enable_signals();
	exec_cmd(ast, &cmd_context);

end:
	free_context(&cmd_context);
}
