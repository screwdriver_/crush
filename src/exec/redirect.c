#include "exec.h"

static int get_default_io(const token_type_t type)
{
	switch(type)
	{
		case LESS_T: return 0;
		case GREAT_T: return 1;
		case DLESS_T: return 0;
		case DGREAT_T: return 1;
		case LESSAND_T: return 0;
		case GREATAND_T: return 1;
		case LESSGREAT_T: return 0;
		case DLESSDASH_T: return 0;
		case CLOBBER_T: return 1;
		default: return -1;
	}
}

static int handle_redirect(ast_t *node)
{
	token_t *tok;
	int io_number = -1;
	token_type_t type;

	tok = node->first_token;
	if(tok->type == IO_NUMBER_T)
	{
		io_number = atoi(tok->data);
		tok = tok->next;
	}

	type = tok->type;
	if(io_number < 0)
		io_number = get_default_io(type);

	tok = tok->next;
	return perform_redirect(io_number, type, tok->data);
}

int redirect(ast_t *node)
{
	node = node->children;
	while(node)
	{
		if(node->type == IO_REDIRECT_A && handle_redirect(node->children) < 0)
			return -1;
		node = node->next;
	}
	return 0;
}
