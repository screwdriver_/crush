#include "hash.h"

__attribute__((section("bss")))
static element_t *buckets[MAX_BUCKETS];

static size_t hash(const char *str)
{
	size_t sum;

	for(sum = 0; *str; ++str)
		sum ^= *str;
	return sum % MAX_BUCKETS;
}

element_t **get_buckets(void)
{
	return buckets;
}

static void free_elements(element_t *e)
{
	element_t *next;

	while(e)
	{
		next = e->next;
		free((void *) e->key);
		free((void *) e->value);
		free((void *) e);
		e = next;
	}
}

void hash_clear(void)
{
	size_t i;

	for(i = 0; i < MAX_BUCKETS; ++i)
		free_elements(buckets[i]);
	bzero(buckets, sizeof(buckets));
}

const char *hash_get(const char *key)
{
	element_t *e;

	if(!key)
		return NULL;
	e = buckets[hash(key)];
	while(e)
	{
		if(strcmp(e->key, key) == 0)
			return e->value;
		e = e->next;
	}
	return NULL;
}

static element_t *alloc_element(const char *key, const char *value)
{
	element_t *e;

	if(!(e = mem_alloc(sizeof(element_t))))
		return NULL;
	if(!(e->key = c_strdup(key)) || !(e->value = c_strdup(value)))
	{
		free((void *)e->key);
		free((void *)e);
		return NULL;
	}
	return e;
}

void hash_set(const char *key, const char *value)
{
	element_t **e, *tmp;

	if(!key || !value)
		return;
	tmp = *(e = buckets + hash(key));
	while(tmp)
	{
		if(strcmp(tmp->key, key) == 0)
		{
			free((void *) tmp->value);
			tmp->value = c_strdup(value);
			return;
		}
		tmp = tmp->next;
	}
	if(!(tmp = alloc_element(key, value)))
		return;
	tmp->next = *e;
	*e = tmp;
}
