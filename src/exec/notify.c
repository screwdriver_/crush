#include "exec.h"

// TODO Print on STDERR?

static void notify(job_t *job)
{
	if(!job || job->notified)
		return;
	if(job_is_complete(job))
	{
		// TODO
		println(STDOUT_FILENO, "TODO: completed notification");
		job_remove(job);
	}
	else if(job_is_stopped(job))
	{
		// TODO
		println(STDOUT_FILENO, "TODO: suspended notification");
	}
	job->notified = 1;
}

void notify_all(void)
{
	job_t *j, *next;

	j = first_job;
	while(j)
	{
		next = j->next;
		if(!j->notified)
			notify(j);
		j = next;
	}
}
