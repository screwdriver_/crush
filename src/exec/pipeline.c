#include "exec.h"

__attribute__((noreturn))
static void exec_cmd(ast_t *ast, exec_context_t *context,
	command_context_t *cmd_context)
{
	if(redirect(ast) < 0)
		exit(1);
	if(cmd_context->builtin)
		exit(cmd_context->builtin->func(context, cmd_context));
	else
		exit(execve(cmd_context->bin_path, (char *const *) cmd_context->argv,
			(char *const *) env_array(cmd_context->environ)));
}

static void e_pipe(ast_t *ast, exec_context_t *context,
	job_t *job, int *pipe_in)
{
	command_context_t cmd_context;
	int pipe_fds[2];
	pid_t pid;
	process_t *process;

	if(prepare_context(ast, &cmd_context) < 0)
	{
		exit_code = 1;
		goto end;
	}
	if(!cmd_context.argv || !cmd_context.argv[0])
	{
		exit_code = 0;
		goto end;
	}
	if(!cmd_context.builtin && !cmd_context.bin_path)
	{
		command_not_found(cmd_context.argv[0]);
		exit_code = 127;
		goto end;
	}

	if(ast->next && c_pipe(pipe_fds) < 0)
	{
		exit_code = 1;
		goto end;
	}
	if((pid = c_fork()) < 0)
	{
		close(pipe_fds[0]);
		close(pipe_fds[1]);
		exit_code = 1;
		goto end;
	}
	if(pid)
	{
		if(*pipe_in > 0)
			close(*pipe_in);
		if(ast->next)
		{
			*pipe_in = pipe_fds[0];
			close(pipe_fds[1]);
		}
		else
			close(pipe_fds[0]);
		if(!(process = process_new(cmd_context.argv, pid))
			|| job_add_process(job, process) < 0)
		{
			free((void *) process);
			// TODO Error?
			exit_code = 1;
		}
		goto end;
	}
	enable_signals();
	if(*pipe_in > 0)
		dup2(*pipe_in, STDIN_FILENO);
	if(ast->next)
	{
		close(pipe_fds[0]);
		dup2(pipe_fds[1], STDOUT_FILENO);
	}
	else
		close(pipe_fds[1]);
	setpgid(0, job->pgid);
	exec_cmd(ast, context, &cmd_context);

end:
	free_context(&cmd_context);
}

void e_pipeline(ast_t *ast, exec_context_t *context)
{
	job_t *job;
	int bang = 0;
	int pipe_in;

	if(!ast || !context || ast->type != PIPELINE_A)
		return;
	if(!(ast = ast->children))
		return;
	if(ast->type == BANG_A)
	{
		bang = 1;
		ast = ast->next;
	}
	if(!ast->next)
	{
		e_simple_command(ast, context);
		goto end;
	}
	if(!(job = job_new(0, ast)))
	{
		// TODO Set exit_code
		goto end;
	}
	pipe_in = STDIN_FILENO;
	while(ast)
	{
		e_pipe(ast, context, job, &pipe_in);
		ast = ast->next;
		if(!ast)
			break;
		if(ast->type == PIPE_A)
			ast = ast->next;
	}
	if(!context->bg)
	{
		job_foreground(job, 0);
		job_remove(job);
	}
	else
		job_foreground(job, 0);

end:
	if(bang)
		exit_code = !exit_code;
}
