#include "exec.h"

void e_and_or(ast_t *ast, exec_context_t *context)
{
	if(!ast || !context || ast->type != AND_OR_A)
		return;
	if(!(ast = ast->children))
		return;
	e_pipeline(ast, context);
	while(ast)
	{
		if((ast->type == AND_IF_A && exit_code != 0)
			|| (ast->type == OR_IF_A && exit_code == 0))
		{
			ast = ast->next;
			if(ast)
				ast = ast->next;
			continue;
		}
		ast = ast->next;
		e_pipeline(ast, context);
	}
}
