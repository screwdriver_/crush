#include "exec.h"

job_t *first_job = NULL;
job_t *current_job = NULL, *previous_job = NULL;

static void free_processes(process_t *p)
{
	process_t *next;

	while(p)
	{
		next = p->next;
		process_remove(p);
		p = next;
	}
}

static void job_free(job_t *job)
{
	if(!job)
		return;
	free((void *) job->command);
	free_processes(job->first_process);
	free((void *) job);
}

job_t *job_new(pid_t pgid, ast_t *node)
{
	job_t *job;

	if(!(job = mem_alloc(sizeof(job_t))))
		return NULL;
	if(first_job)
		job->id = first_job->id + 1;
	else
		job->id = 1;
	job->command = job_get_command(node);
	job->pgid = pgid;
	job->next = first_job;
	first_job = job;
	return job;
}

void job_remove(job_t *job)
{
	job_t *tmp, *prev = NULL;

	if(!job || !job_is_complete(job))
		return;
	tmp = first_job;
	while(tmp && tmp != job)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	if(prev)
		prev->next = tmp->next;
	else
		first_job = tmp->next;
	if(current_job == job)
		current_job = NULL;
	if(previous_job == job)
		previous_job = NULL;
	job_free(tmp);
}

job_t *job_find(const pid_t pgid)
{
	job_t *j;

	for(j = first_job; j; j = j->next)
		if(j->pgid == pgid)
			return j;
	return NULL;
}

process_t *get_process(const pid_t pid)
{
	job_t *j;
	process_t *p;

	j = first_job;
	while(j)
	{
		if((p = job_get_process(j, pid)))
			return p;
		j = j->next;
	}
	return NULL;
}

process_t *job_get_process(job_t *job, const pid_t pid)
{
	process_t *p;

	if(!job)
		return NULL;
	p = job->first_process;
	while(p && p->pid != pid)
		p = p->next;
	return p;
}

int job_add_process(job_t *job, process_t *process)
{
	process_t *p;

	if(!job || !process)
		return -1;
	if(job->pgid == 0)
		job->pgid = process->pid;
	setpgid(process->pid, job->pgid);
	if(!(p = job->first_process))
	{
		job->first_process = process;
		return 0;
	}
	while(p->next)
		p = p->next;
	p->next = process;
	return 0;
}

void job_foreground(job_t *job, const int cont)
{
	if(!job)
		return;
	tcsetpgrp(shell_terminal, job->pgid);
	if(cont)
	{
		tcsetattr(shell_terminal, TCSADRAIN, &job->tmodes);
		if(kill(-job->pgid, SIGCONT) < 0)
		{
			// TODO Error?
		}
	}
	wait_job(job);
	tcsetpgrp(shell_terminal, shell_pgid);
	tcgetattr(shell_terminal, &job->tmodes);
}

void job_background(job_t *job, const int cont)
{
	if(!job)
		return;
	if(cont && kill(-job->pgid, SIGCONT) < 0)
	{
		// TODO Error?
	}
}

int job_is_stopped(job_t *job)
{
	process_t *p;

	if(!job)
		return 0;
	for(p = job->first_process; p; p = p->next)
		if(!p->completed && !p->stopped)
			return 0;
	return 1;
}

int job_is_complete(job_t *job)
{
	process_t *p;

	if(!job)
		return 1;
	for(p = job->first_process; p; p = p->next)
		if(!p->completed)
			return 0;
	return 1;
}
