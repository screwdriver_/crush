#include "exec.h"

void e_complete_command(ast_t *ast, exec_context_t *context)
{
	ast_t *node;

	if(!ast || !context || ast->type != COMPLETE_COMMAND_A)
		return;
	if(!(ast = ast->children))
		return;
	while(ast)
	{
		node = ast;
		ast = ast->next;
		context->bg = 0;
		if(ast && ast->type == SEPARATOR_OP_A)
		{
			// TODO Set context->bg if background
			ast = ast->next;
		}
		e_and_or(node, context);
	}
}
