#include "exec.h"

void exec(ast_t *ast, exec_context_t *context)
{
	if(!ast || !context || ast->type != PROGRAM_A)
		return;
	handle_heredocs(ast, context);
	if(!(ast = ast->children))
		return;
	while(ast)
	{
		e_complete_command(ast, context);
		ast = ast->next;
	}
	free_heredocs(context);
}
