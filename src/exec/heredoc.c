#include "exec.h"
#include "../prompt/prompt.h"

static int is_heredoc_end(const char *line, const char *here_end)
{
	while(*line && *here_end && *line == *here_end)
	{
		++line;
		++here_end;
	}
	return (!*here_end && *line == '\n');
}

static void do_heredoc(ast_t *ast, exec_context_t *context)
{
	const char *here_end;
	heredoc_t *h;
	int fd;
	const char *buff = NULL;

	// TODO Handle DLESSDASH
	here_end = ast->first_token->next->data;
	if(!(h = mem_alloc(sizeof(heredoc_t))))
		return;
	if(!(h->file = new_tmp_file()))
	{
		free((void *) h);
		return;
	}

	if((fd = open(h->file, O_CREAT | O_WRONLY, 0644)) < 0)
	{
		free((void *) h->file);
		free((void *) h);
		return;
	}
	while(1)
	{
		free((void *) buff);
		if(!(buff = prompt("PS2"))) // TODO Ctrl-C -> cancel command; Ctrl-D -> end input and continue
			break;
		if(is_heredoc_end(buff, here_end))
			break;
		write(fd, buff, strlen(buff));
	}
	free((void *) buff);
	close(fd);

	free((void *) ast->first_token->next->data);
	ast->first_token->next->data = c_strdup(h->file);
	h->next = context->heredocs;
	context->heredocs = h;
}

void handle_heredocs(ast_t *ast, exec_context_t *context)
{
	if(!ast)
		return;
	if(ast->type == IO_HERE_A)
		do_heredoc(ast, context);
	while(ast)
	{
		handle_heredocs(ast->children, context);
		ast = ast->next;
	}
}

void free_heredocs(exec_context_t *context)
{
	heredoc_t *h, *next;

	h = context->heredocs;
	while(h)
	{
		next = h->next;
		remove(h->file);
		free((void *) h);
		h = next;
	}
	context->heredocs = NULL;
}
