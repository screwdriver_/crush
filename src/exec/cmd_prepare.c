#include "exec.h"

static size_t count_words(ast_t *node)
{
	size_t n;

	n = 0;
	while(node && node->type == WORD_A)
	{
		if(node->first_token && node->first_token->data)
			++n;
		node = node->next;
	}
	return n;
}

static int prepare_argv(ast_t *node, command_context_t *cmd_context)
{
	size_t word_count;
	size_t i;

	while(node && node->type != WORD_A)
		node = node->next;
	word_count = count_words(node);
	if(!(cmd_context->argv = mem_alloc(sizeof(char *) * (word_count + 1))))
		return -1;
	for(i = 0; node && i < word_count; ++i, node = node->next)
		if(node->first_token && node->first_token->data)
			cmd_context->argv[i] = node->first_token->data;
	return 0;
}

static int is_only_assignments(ast_t *node)
{
	if(!node || node->type != ASSIGNMENT_WORD_A)
		return 0;
	while(node && node->type == ASSIGNMENT_WORD_A)
		node = node->next;
	return !(node && node->type == WORD_A);
}

static int prepare_environ(ast_t *node, command_context_t *cmd_context)
{
	var_t **v;

	// TODO only if not in a pipeline and not in background
	if(!is_only_assignments(node))
	{
		if(!(cmd_context->environ = var_dup(vars)))
			return -1;
		v = &cmd_context->environ;
	}
	else
		v = &vars;
	while(node && node->type == ASSIGNMENT_WORD_A)
	{
		var_assign(v, node->first_token->data);
		// TODO Reset hash if PATH is assigned
		node = node->next;
	}
	return 0;
}

static const char *get_path(var_t *vars)
{
	var_t *v;

	if(!(v = var_get(vars, "PATH")))
		return NULL;
	return v->value;
}

int prepare_context(ast_t *ast, command_context_t *cmd_context)
{
	ast_t *node;
	const char *cmd_name, *path;

	if(!ast || !cmd_context)
		return -1;
	bzero(cmd_context, sizeof(command_context_t));
	node = ast->children;
	if(prepare_argv(node, cmd_context) < 0)
		return -1;
	cmd_name = cmd_context->argv[0];
	if(prepare_environ(node, cmd_context) < 0)
		return -1;
	path = get_path(cmd_context->environ);
	if(!(cmd_context->builtin = get_builtin(cmd_name)))
		cmd_context->bin_path = get_bin_path(path, cmd_name);
	// TODO Check if every redirection is valid and file exist (fail if not)
	return 0;
}

void free_context(command_context_t *cmd_context)
{
	free((void *) cmd_context->argv);
	var_freeall(cmd_context->environ);
	free((void *) cmd_context->bin_path);
}
