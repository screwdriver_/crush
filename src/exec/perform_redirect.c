#include "exec.h"

static int link_file(const int io_number, const char *word, const int flags)
{
	int fd;

	if((fd = open(word, flags | O_CREAT, 0644)) < 0)
		return -1;
	if(dup2(fd, io_number) < 0)
		return -1;
	return 0;
}

int perform_lessand(const int io_number, const char *word)
{
	// TODO
	(void) io_number;
	(void) word;
	return 0;
}

int perform_greatand(const int io_number, const char *word)
{
	// TODO
	(void) io_number;
	(void) word;
	return 0;
}

// TODO noclobber
int perform_redirect(const int io_number, const token_type_t type,
	const char *word)
{
	switch(type)
	{
		case LESS_T: return link_file(io_number, word, O_RDONLY);
		case GREAT_T: return link_file(io_number, word, O_WRONLY);
		case DLESS_T: return link_file(io_number, word, O_RDONLY);
		case DGREAT_T: return link_file(io_number, word, O_WRONLY | O_APPEND);
		case LESSAND_T: return perform_lessand(io_number, word);
		case GREATAND_T: return perform_greatand(io_number, word);
		case LESSGREAT_T: return link_file(io_number, word, O_RDWR);
		case DLESSDASH_T: return link_file(io_number, word, O_RDONLY);
		case CLOBBER_T: return link_file(io_number, word, O_RDONLY);
		default: return -1;
	}
}
