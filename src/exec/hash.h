#ifndef HASH_H
# define HASH_H

# include "../crush.h"

# define MAX_BUCKETS	1024

typedef struct element
{
	struct element *next;

	const char *key;
	const char *value;
} element_t;

element_t **get_buckets(void);
void hash_clear(void);
const char *hash_get(const char *key);
void hash_set(const char *key, const char *value);

#endif
