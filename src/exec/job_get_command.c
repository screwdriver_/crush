#include "exec.h"
#include "../string_part.h"

// TODO Handle every nodes
static void handle_node(string_part_t **parts, ast_t *node)
{
	if(!node)
		return;
	if(node->type == SIMPLE_COMMAND_A || node->type == PIPELINE_A)
		return handle_node(parts, node->children);
	// TODO
	handle_node(parts, node->next);
}

const char *job_get_command(ast_t *node)
{
	string_part_t *parts = NULL;

	if(!node)
		return NULL;
	handle_node(&parts, node);
	// TODO Join all
	return strdup("TODO");
}
