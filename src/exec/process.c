#include "exec.h"

process_t *process_new(const char **argv, const pid_t pid)
{
	process_t *proc;

	if(!argv || !pid)
		return NULL;
	if(!(proc = mem_alloc(sizeof(process_t))))
		return NULL;
	proc->argv = argv;
	proc->pid = pid;
	return proc;
}

void process_remove(process_t *process)
{
	if(!process)
		return;
	// TODO Free stuff in `process`?
	free((void *) process);
}
