#include "exec.h"

static job_t *iterate_jobs(const char *str,
	int (*is_selected)(const char *, job_t *))
{
	job_t *j;

	j = first_job;
	while(j)
	{
		if(is_selected(str, j))
			return j;
		j = j->next;
	}
	return j;
}

static int by_id(const char *str, job_t *job)
{
	return (job->id == atoi(str));
}

static int by_name(const char *str, job_t *job)
{
	return (strcmp(job->command, str) == 0);
}

static int name_contains(const char *str, job_t *job)
{
	return (strstr(job->command, str) != NULL);
}

job_t *job_get(const char *expr)
{
	const char *str;
	int (*f)(const char *, job_t *) = NULL;

	if(!expr || strcmp(expr, "%%") == 0 || strcmp(expr, "%+") == 0)
		return current_job;
	else if(strcmp(expr, "%-") == 0)
		return previous_job;
	else if(*expr == '%' && is_nbr(expr + 1))
	{
		str = expr + 1;
		f = by_id;
	}
	else if(expr[0] == '%' && expr[1])
	{
		str = expr + 1;
		f = by_name;
	}
	else if(expr[0] == '%' && expr[1] == '?' && expr[2])
	{
		str = expr + 2;
		f = name_contains;
	}
	else
	{
		str = expr;
		f = by_name;
	}
	return iterate_jobs(str, f);
}
