#include "exec.h"

static void print_signal_message(const int sig)
{
	switch(sig)
	{
		case SIGABRT:
		{
			crash("Abort");
			break;
		}

		case SIGBUS:
		{
			crash("Bus error");
			break;
		}

		case SIGFPE:
		{
			crash("Floating point exception");
			break;
		}

		case SIGSEGV:
		{
			crash("Segmentation fault");
			break;
		}
	}
}

static int get_exit_code(const int status)
{
	int sig;

	if(WIFSIGNALED(status))
	{
		print_signal_message(sig = WTERMSIG(status));
		return (128 + sig);
	}
	else if(WIFEXITED(status))
		return (WEXITSTATUS(status));
	else if(WIFSTOPPED(status))
		return (WSTOPSIG(status));
	return (1);
}

static int mark_process_status(const pid_t pid, const int status)
{
	process_t *process;

	if(!(process = get_process(pid)))
		return -1;
	process->status = status;
	if(WIFSTOPPED(status))
		process->stopped = 1;
	else
		process->completed = 1;
	return 0;
}

int wait_process(process_t *process)
{
	int status;
	job_t *job;

	if(!process)
		return 0;
	if(waitpid(process->pid, &status, WUNTRACED) < 0)
		return -1; // TODO Error?
	mark_process_status(process->pid, status);
	if(process->stopped)
	{
		if(!(job = job_new(process->pid, NULL))) // TODO
			return -1;
		job_add_process(job, process);
		return 0;
	}
	else
		return (exit_code = get_exit_code(status));
}

int wait_job(job_t *job)
{
	int status = 0;
	pid_t pid;

	if(!job)
		return 0;
	do
		pid = waitpid(-job->pgid, &status, WUNTRACED);
	while(!mark_process_status(pid, status)
		&& !job_is_stopped(job) && !job_is_complete(job));
	return (exit_code = get_exit_code(status));
}
