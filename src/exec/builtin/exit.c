#include "builtin.h"

// TODO Trap
int builtin_exit(exec_context_t *exec, command_context_t *cmd)
{
	int code;

	(void) exec;
	code = exit_code;
	if(cmd->argv[1])
	{
		if(!is_nbr(cmd->argv[1]))
		{
			builtin_error("exit", "numeric argument required");
			exit(1);
		}
		code = atoi(cmd->argv[1]);
		if(cmd->argv[2])
		{
			builtin_error("exit", "too many arguments");
			return 1;
		}
	}
	exit(code);
}
