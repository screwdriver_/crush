#include "builtin.h"

int builtin_alias(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	char *s;
	int err = 0;
	alias_t *alias;

	(void) exec;
	argv = cmd->argv + 1;
	if(!*argv)
	{
		alias_print_all();
		return 0;
	}
	while(*argv)
	{
		if(*(s = strchr(*argv, '=')))
		{
			*s = '\0';
			alias_set(*argv, s + 1);
		}
		else if(!(alias = alias_get(*argv)))
		{
			print(STDERR_FILENO, "crush: alias: ");
			print(STDERR_FILENO, *argv);
			print(STDERR_FILENO, " not found\n");
			err = 1;
		}
		else
			alias_print(alias);
		++argv;
	}
	return err;
}
