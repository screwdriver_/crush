#ifndef BUILTIN_H
# define BUILTIN_H

# include "../exec.h"

typedef struct exec_context exec_context_t;
typedef struct command_context command_context_t;

typedef struct builtin
{
	const char *name;
	int (*func)(exec_context_t *, command_context_t *);
} builtin_t;

builtin_t *get_builtin(const char *name);

int builtin_colon(exec_context_t *exec, command_context_t *cmd);
int builtin_alias(exec_context_t *exec, command_context_t *cmd);
int builtin_bg(exec_context_t *exec, command_context_t *cmd);
int builtin_cd(exec_context_t *exec, command_context_t *cmd);
int builtin_eval(exec_context_t *exec, command_context_t *cmd);
int builtin_exit(exec_context_t *exec, command_context_t *cmd);
int builtin_false(exec_context_t *exec, command_context_t *cmd);
int builtin_fg(exec_context_t *exec, command_context_t *cmd);
int builtin_jobs(exec_context_t *exec, command_context_t *cmd);
int builtin_hash(exec_context_t *exec, command_context_t *cmd);
int builtin_set(exec_context_t *exec, command_context_t *cmd);
int builtin_source(exec_context_t *exec, command_context_t *cmd);
int builtin_true(exec_context_t *exec, command_context_t *cmd);
int builtin_unset(exec_context_t *exec, command_context_t *cmd);
int builtin_unalias(exec_context_t *exec, command_context_t *cmd);

void job_not_found(const char *cmd, const char *expr);

#endif
