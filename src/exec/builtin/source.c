#include "builtin.h"

static inline void usage(void)
{
	println(STDERR_FILENO, "source: usage: source <file>");
}

int builtin_source(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;

	(void) exec;
	argv = cmd->argv;
	if(!argv[1])
	{
		usage();
		return 2;
	}
	if(argv[2])
	{
		builtin_error("source", "too many arguments");
		return 2;
	}
	if(access(argv[1], R_OK) < 0)
	{
		builtin_error("source", "can't read file");
		return 2;
	}
	exec_source(argv[1]);
	return exit_code;
}
