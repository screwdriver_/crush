#include "builtin.h"

int builtin_false(exec_context_t *exec, command_context_t *cmd)
{
	(void) exec;
	(void) cmd;
	return 1;
}
