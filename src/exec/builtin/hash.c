#include "builtin.h"
#include "../hash.h"

static inline void usage(void)
{
	println(STDERR_FILENO, "hash: usage: hash (-r|[utility...])");
}

static void list_hash(void)
{
	size_t i;
	element_t **e, *tmp;

	e = get_buckets();
	for(i = 0; i < MAX_BUCKETS; ++i)
	{
		if(e[i])
		{
			tmp = e[i];
			while(tmp)
			{
				println(STDOUT_FILENO, tmp->value);
				tmp = tmp->next;
			}
		}
	}
}

static void add_utilities(command_context_t *cmd, const char **utilities)
{
	var_t *path_var;
	const char *path = NULL;
	const char *util_path;

	if((path_var = var_get(cmd->environ, "PATH")))
		path = path_var->value;
	while(*utilities)
	{
		if((util_path = path_find(path, *utilities)))
			hash_set(*utilities, util_path);
		else
		{
			print(STDERR_FILENO, "hash: ");
			print(STDERR_FILENO, *utilities);
			println(STDERR_FILENO, ": not found");
		}
		++utilities;
	}
}

int builtin_hash(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;

	(void) exec;
	argv = cmd->argv;
	if(!argv[1])
	{
		list_hash();
		return 0;
	}
	if(strcmp(argv[1], "-r") == 0)
	{
		if(cmd->argv[2])
		{
			usage();
			return 2;
		}
		hash_clear();
		return 0;
	}
	if(strcmp(argv[1], "--") == 0)
		++argv;
	add_utilities(cmd, argv + 1);
	return 0;
}
