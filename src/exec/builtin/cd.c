#include "builtin.h"

static void var_not_set(const char *var)
{
	print(STDERR_FILENO, "crush: cd: ");
	print(STDERR_FILENO, var);
	println(STDERR_FILENO, " not set");
}

static void set_curpath(const char *path)
{
	var_t *v;

	if(!path || !(v = var_get(vars, "PWD")))
		return;
	var_set(&vars, "OLDPWD", v->value);
	var_set(&vars, "PWD", path);
	chdir(path);
}

int builtin_cd(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	var_t *var;
	const char *dir = NULL;

	(void) exec;
	argv = cmd->argv + 1;
	// TODO Parse `-L` and `-P`
	if(!*argv)
	{
		if(!(var = var_get(vars, "HOME")))
		{
			var_not_set("HOME");
			return 1;
		}
		dir = var->value;
	}
	else if(strcmp(*argv, "-") == 0)
	{
		if(!(var = var_get(vars, "OLDPWD")))
		{
			var_not_set("OLDPWD");
			return 1;
		}
		dir = var->value;
	}
	else
		dir = *argv;
	// TODO
	set_curpath(dir);
	return 0;
}
