#include "builtin.h"

static size_t get_length(const char **args)
{
	size_t n = 0;

	while(*args)
	{
		n += strlen(args[0]);
		if(args[1])
			++n;
		++args;
	}
	return n;
}

int builtin_eval(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	size_t len;
	char *cmdline;
	size_t i = 0, l;
	int status;

	(void) exec;
	argv = cmd->argv + 1;
	len = get_length(argv);
	if(!(cmdline = mem_alloc(len + 1)))
		return 127;
	while(*argv)
	{
		l = strlen(argv[0]);
		memcpy(cmdline + i, argv[0], l);
		i += l;
		if(argv[1])
			cmdline[i++] = ' ';
		++argv;
	}
	status = exec_command(cmdline);
	free((void *) cmdline);
	return status;
}
