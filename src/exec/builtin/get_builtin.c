#include "builtin.h"

static builtin_t builtins[] = {
	{":", builtin_colon},
	{"alias", builtin_alias},
	{"bg", builtin_bg},
	{"cd", builtin_cd},
	{"eval", builtin_eval},
	{"exit", builtin_exit},
	{"false", builtin_false},
	{"fg", builtin_fg},
	{"jobs", builtin_jobs},
	{"hash", builtin_hash},
	{"set", builtin_set},
	{"source", builtin_source},
	{"true", builtin_true},
	{"unalias", builtin_unalias},
	{"unset", builtin_unset}
	// TODO
};

builtin_t *get_builtin(const char *name)
{
	size_t i;

	if(!name)
		return NULL;
	for(i = 0; i < sizeof(builtins) / sizeof(builtin_t); ++i)
	{
		if(strcmp(builtins[i].name, name) == 0)
			return builtins + i;
	}
	return NULL;
}
