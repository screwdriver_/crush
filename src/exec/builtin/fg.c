#include "builtin.h"

int builtin_fg(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	const char *job_name = NULL;
	job_t *j;

	(void) exec;
	argv = cmd->argv + 1;
	if(*argv)
		job_name = *argv;
	if(!(j = job_get(job_name)))
	{
		job_not_found("fg", job_name);
		return 1;
	}
	job_foreground(j, 1);
	return 0;
}
