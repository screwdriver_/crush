#include "builtin.h"

int builtin_unalias(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	alias_t *alias;
	int err = 0;

	(void) exec;
	argv = cmd->argv + 1;
	if(!*argv)
		return 0;
	if(strcmp(*argv, "-a") == 0)
	{
		alias_unset_all();
		return 0;
	}
	while(*argv)
	{
		if(!(alias = alias_get(*argv)))
		{
			print(STDERR_FILENO, "crush: unalias: ");
			print(STDERR_FILENO, *argv);
			print(STDERR_FILENO, " not found\n");
			err = 1;
		}
		else
			alias_unset(alias);
		++argv;
	}
	return err;
}
