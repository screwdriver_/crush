#include "builtin.h"

void job_not_found(const char *cmd, const char *expr)
{
	print(STDERR_FILENO, "crush: ");
	if(cmd)
	{
		print(STDERR_FILENO, cmd);
		print(STDERR_FILENO, ": ");
	}
	print(STDERR_FILENO, (expr ? expr : "current"));
	println(STDERR_FILENO, ": no such job");
}
