#include "builtin.h"
#include "../../crush.h"

static void unset_var(const char **argv)
{
	while(*argv)
	{
		var_unset(&vars, *argv);
		++argv;
	}
}

static void unset_func(const char **argv)
{
	(void) argv;
	// TODO
}

int builtin_unset(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	int func = 0;

	(void) exec;
	argv = cmd->argv;
	if(!argv[1])
		return 0;
	if(strcmp(argv[1], "-v") == 0)
	{
		++argv;
		func = 0;
	}
	else if(strcmp(argv[1], "-f") == 0)
	{
		++argv;
		func = 1;
	}
	if(strcmp(argv[1], "--") == 0)
		++argv;
	if(!func)
		unset_var(argv + 1);
	else
		unset_func(argv + 1);
	return 0;
}
