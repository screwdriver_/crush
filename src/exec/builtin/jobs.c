#include "builtin.h"

#define ERR_OPTION	1
#define L_OPTION	2
#define P_OPTION	3

static inline void usage(void)
{
	println(STDERR_FILENO, "jobs: usage: jobs [-l|-p] [job_id...]");
}

static int parse_options(const char ***argv)
{
	int option = 0;
	const char *s;

	while(**argv && ***argv == '-' && strcmp(**argv, "--") != 0)
	{
		s = **argv;
		while(*s)
		{
			if(*s == 'l')
				option = L_OPTION;
			else if(*s == 'p')
				option = P_OPTION;
			else
			{
				builtin_error("jobs", "invalid option"); // TODO Specify which one?
				usage();
				return ERR_OPTION;
			}
			++s;
		}
		++(*argv);
	}
	return option;
}

static void print_job_prefix(job_t *job)
{
	printchar(STDOUT_FILENO, '[');
	printnbr(STDOUT_FILENO, job->id);
	printchar(STDOUT_FILENO, ']');
	if(job == current_job)
		printchar(STDOUT_FILENO, '+');
	else if(job == previous_job)
		printchar(STDOUT_FILENO, '-');
	else
		printchar(STDOUT_FILENO, ' ');
	printchar(STDOUT_FILENO, ' ');
}

static void print_job(job_t *job, const int option)
{
	if(!job || job_is_complete(job))
		return;
	if(option == P_OPTION)
		printnbr(STDOUT_FILENO, job->pgid);
	else if(option == L_OPTION)
	{
		print_job_prefix(job);
		printnbr(STDOUT_FILENO, job->pgid);
		printchar(STDOUT_FILENO, ' ');
		// TODO Print state
		print(STDOUT_FILENO, job->command);
	}
	else
	{
		print_job_prefix(job);
		// TODO Print state
		print(STDOUT_FILENO, job->command);
	}
	println(STDOUT_FILENO, NULL);
}

static void print_all_jobs(const int option)
{
	job_t *j;

	j = first_job;
	while(j)
	{
		print_job(j, option);
		j = j->next;
	}
}

int builtin_jobs(exec_context_t *exec, command_context_t *cmd)
{
	const char **argv;
	int option;

	(void) exec;
	argv = cmd->argv + 1;
	if((option = parse_options(&argv)) == ERR_OPTION)
		return 2;
	if(!*argv)
	{
		print_all_jobs(option);
		return 0;
	}
	while(*argv)
	{
		print_job(job_get(*argv), option);
		++argv;
	}
	return 0;
}
