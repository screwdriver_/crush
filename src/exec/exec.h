#ifndef EXEC_H
# define EXEC_H

# include <sys/wait.h>

# include "../parser/parser.h"
# include "builtin/builtin.h"

typedef struct heredoc
{
	struct heredoc *next;
	const char *file;
} heredoc_t;

typedef struct process
{
	struct process *next;

	const char **argv;
	pid_t pid;
	char completed;
	char stopped;
	int status;
} process_t;

typedef struct job
{
	struct job *next;

	int id;
	const char *command;
	process_t *first_process;
	pid_t pgid;
	char notified;
	struct termios tmodes;
} job_t;

typedef struct exec_context
{
	heredoc_t *heredocs;

	int bg;
	job_t *job;
} exec_context_t;

typedef struct builtin builtin_t;

typedef struct command_context
{
	const char **argv;
	var_t *environ;

	const char *bin_path;
	builtin_t *builtin;
} command_context_t;

extern job_t *first_job;
extern job_t *current_job;
extern job_t *previous_job;

void exec(ast_t *ast, exec_context_t *context);
void e_complete_command(ast_t *ast, exec_context_t *context);
void e_and_or(ast_t *ast, exec_context_t *context);
void e_pipeline(ast_t *ast, exec_context_t *context);
void e_simple_command(ast_t *ast, exec_context_t *context);

void handle_heredocs(ast_t *ast, exec_context_t *context);
void free_heredocs(exec_context_t *context);

int prepare_context(ast_t *ast, command_context_t *cmd_context);
void free_context(command_context_t *cmd_context);

const char *get_bin_path(const char *path, const char *name);
const char *path_find(const char *path, const char *bin);

int redirect(ast_t *node);
int perform_redirect(int io_number, token_type_t type, const char *word);

process_t *process_new(const char **argv, pid_t pid);
void process_remove(process_t *process);

job_t *get_jobs(void);
const char *job_get_command(ast_t *node);
job_t *job_new(pid_t pgid, ast_t *node);
void job_remove(job_t *job);
job_t *job_find(pid_t pgid);
job_t *job_get(const char *expr);
process_t *get_process(pid_t pid);
process_t *job_get_process(job_t *job, pid_t pid);
int job_add_process(job_t *job, process_t *process);
void job_foreground(job_t *job, int cont);
void job_background(job_t *job, int cont);
int job_is_stopped(job_t *j);
int job_is_complete(job_t *j);

int wait_process(process_t *process);
int wait_job(job_t *job);

void notify_all(void);

#endif
