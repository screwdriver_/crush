#include "exec.h"
#include "hash.h"

static const char *get_full_path(const char *path_begin, const char *path_end,
	const char *bin)
{
	size_t len;
	size_t bin_len;
	char *buff;

	if(!path_begin || !bin || path_begin > path_end)
		return NULL;
	len = path_end - path_begin;
	bin_len = strlen(bin);
	if(!(buff = mem_alloc(len + bin_len + 2)))
		return NULL;
	memcpy(buff, path_begin, len);
	buff[len] = '/';
	memcpy(buff + len + 1, bin, bin_len);
	return buff;
}

static inline int file_exists(const char *path)
{
	return (access(path, F_OK) == 0);
}

static const char *get_absolute_path(const char *path)
{
	char buffer[PATH_MAX + 1];

	bzero(buffer, sizeof(buffer));
	if(!realpath(path, buffer))
		return NULL;
	if(!file_exists(buffer))
		return NULL;
	return c_strdup(buffer);
}

const char *path_find(const char *path, const char *bin)
{
	const char *s;
	const char *full_path;

	if(!path || !bin)
		return NULL;
	while(1)
	{
		s = strchr(path, ':');
		if(!(full_path = get_full_path(path, s, bin)))
			return NULL;
		if(file_exists(full_path))
			return full_path;
		free((void *) full_path);
		if(!s)
			break;
		path = s + 1;
	}
	return NULL;
}

const char *get_bin_path(const char *path, const char *name)
{
	const char *bin_path;

	if(!name)
		return NULL;
	if(name[0] == '/')
		return c_strdup(name);
	if(name[0] == '.')
		return get_absolute_path(name);
	if((bin_path = hash_get(name)))
		return c_strdup(bin_path); // TODO Test if exists?
	if((bin_path = path_find(path, name)))
	{
		hash_set(name, bin_path);
		return bin_path;
	}
	return NULL;
}
