#include "crush.h"
#include "prompt/readline.h"
#include "exec/exec.h"
#include "exec/hash.h"

pid_t shell_pgid;
int shell_terminal;
int shell_is_interactive;

var_t *vars = NULL;

static void exec_default_source(void)
{
	var_t *home;
	const char *path;

	if(!(home = var_get(vars, "HOME")))
		return;
	if(!(path = merge_path(home->value, ".crushrc")))
		return;
	exec_source(path);
	free((void *) path);
}

static void init_shell(const int interactive)
{
	shell_pgid = getpid();
	shell_terminal = STDIN_FILENO;
	if(!(shell_is_interactive = interactive	&& isatty(shell_terminal)))
		return;

	while(tcgetpgrp(shell_terminal) != (shell_pgid = getpgrp()))
		kill(-shell_pgid, SIGTTIN);
	disable_signals();
	shell_pgid = getpid();
	if(setpgid(shell_pgid, shell_pgid) < 0)
		fatal_error("Failed to put shell in its own process group!");
	tcsetpgrp(shell_terminal, shell_pgid);
}

int main(int argc, char **argv, char **environ)
{
	const char *input;
	int interactive = 1;

	(void) argc;
	env_load(&vars, environ);
	var_set_defaults(&vars);
	if(argv[1] && strcmp(argv[1], "-s") == 0)
	{
		interactive = 0;
		++argv;
	}
	if(argv[1] && strcmp(argv[1], "-c"))
	{
		// TODO
	}
	// TODO If no `-c` and no `-s` but arguments are present, execute script
	init_shell(interactive);
	exec_default_source();

	if(shell_is_interactive)
	{
		init_prompt();
		while(1)
		{
			if((input = get_command()))
				exec_command(input);
			free((void *) input);
			notify_all();
		}
	}
	else
	{
		while((input = read_line(shell_terminal)))
		{
			exec_command(input);
			free((void *) input);
		}
	}
	return 0;
}
