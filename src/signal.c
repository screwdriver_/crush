#include "crush.h"

void enable_signals(void)
{
	signal(SIGINT, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);
	/*signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);*/
}

void disable_signals(void)
{
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);
	/*signal(SIGTTIN, SIG_IGN);
	signal(SIGTTOU, SIG_IGN);*/
}
