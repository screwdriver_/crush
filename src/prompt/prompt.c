#include "prompt.h"
#include "readline.h"

#define RESET_CURSOR	"\033[1;1H"

static struct termios old_tmodes;

void init_prompt(void)
{
	var_t *term;
	const char *term_str;

	if((term = var_get(vars, "TERM")))
		term_str = term->value;
	else
		term_str = DEFAULT_TERM;
	if(tgetent(NULL, term_str) < 0)
		fatal_error("Cannot access terminfo database!");
	if(tcgetattr(STDIN_FILENO, &old_tmodes) < 0)
		fatal_error("Failed to get terminal attributes!");
}

static void set_tmodes(void)
{
	struct termios shell_tmodes;

	shell_tmodes = old_tmodes;
	shell_tmodes.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR
		| ICRNL | IXON | ECHO);
	shell_tmodes.c_lflag &= ~(ECHO | ICANON | ISIG);
	shell_tmodes.c_cflag |= CS8;
	shell_tmodes.c_cc[VMIN] = 1;
	shell_tmodes.c_cc[VTIME] = 0;
	if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &shell_tmodes) < 0)
		fatal_error("Failed to set terminal attributes!");
}

static void restore_tmodes(void)
{
	if(tcsetattr(STDIN_FILENO, TCSAFLUSH, &old_tmodes) < 0)
		fatal_error("Failed to set terminal attributes!");
}

static inline void clear_terminal(void)
{
	static char *str = NULL;

	if(!str)
		str = termcap_load("cl");
	tputs(str, 1, term_write);
}

static inline void print_ctrl(const char c)
{
	const char buff[3] = {'^', c, '\0'};
	write(STDERR_FILENO, buff, sizeof(buff));
}

static inline void print_newline()
{
	term_write('\n');
}

void print_prompt(const char *prompt_var)
{
	var_t *var;

	if(!shell_is_interactive || !(var = var_get(vars, prompt_var)))
		return;
	// TODO Keep reset_color?
	reset_color(STDERR_FILENO);
	print(STDERR_FILENO, var->value); // TODO Expansions
	reset_color(STDERR_FILENO);
}

static void prompt_prepare(prompt_t *prompt)
{
	get_winsize(&prompt->win_width, &prompt->win_height);
	print_prompt(prompt->prompt_var);
	get_cursor_pos(&prompt->cursor_base_x, &prompt->cursor_base_y);
	write(STDERR_FILENO, prompt->buffer.data, prompt->buffer.length);
	get_cursor_pos(&prompt->cursor_x, &prompt->cursor_y);
}

static void arrow_up(prompt_t *prompt)
{
	// TODO History
	get_cursor_pos(&prompt->cursor_x, &prompt->cursor_y);
}

static void arrow_down(prompt_t *prompt)
{
	// TODO History
	get_cursor_pos(&prompt->cursor_x, &prompt->cursor_y);
}

static void arrow_left(prompt_t *prompt)
{
	if(prompt->cursor_x <= prompt->cursor_base_x
		&& prompt->cursor_y <= prompt->cursor_base_y)
		return;
	if(prompt->curpos <= 0)
		return;
	if(prompt->cursor_x - 1 <= 0)
		set_cursor_pos(prompt->win_width, prompt->cursor_y - 1);
	else
		move_cursor_left();
	get_cursor_pos(&prompt->cursor_x, &prompt->cursor_y);
	--prompt->curpos;
}

static void arrow_right(prompt_t *prompt)
{
	if(prompt->curpos >= prompt->buffer.length)
		return;
	if(prompt->cursor_x + 1 >= (signed) prompt->win_width)
		print_newline();
	else
		move_cursor_right();
	get_cursor_pos(&prompt->cursor_x, &prompt->cursor_y);
	++prompt->curpos;
}

static void handle_escape(prompt_t *prompt)
{
	char buff[16];
	size_t i = 0;

	if(read(STDIN_FILENO, buff, sizeof(buff)) < 0)
		return;
	if(buff[i] == '[')
		++i;
	switch(buff[i++])
	{
		case 'A':
		{
			arrow_up(prompt);
			break;
		}

		case 'B':
		{
			arrow_down(prompt);
			break;
		}

		case 'C':
		{
			arrow_right(prompt);
			break;
		}

		case 'D':
		{
			arrow_left(prompt);
			break;
		}
	}
}

static void handle_del(prompt_t *prompt)
{
	static char *str = NULL;

	if(prompt->cursor_x <= prompt->cursor_base_x
		&& prompt->cursor_y <= prompt->cursor_base_y)
		return;
	arrow_left(prompt);
	if(!str)
		str = termcap_load("dc");
	tputs(str, 1, term_write);
	buffer_erase(&prompt->buffer, prompt->curpos, 1);
}

static int handle_ctrl(prompt_t *prompt, const char c)
{
	switch(c)
	{
		case 3:
		{
			print_ctrl('C');
			print_newline();
			prompt->discard = 1;
			break;
		}

		case 4:
		{
			if(prompt->buffer.length > 0)
				return 0;
			print_newline();
			restore_tmodes();
			exit(exit_code);
		}

		case 12:
		{
			clear_terminal();
			prompt_prepare(prompt);
			return 0;
		}

		case 13:
		{
			print_newline();
			break;
		}

		case 033:
		{
			handle_escape(prompt);
			return 0;
		}

		case 127:
		{
			handle_del(prompt);
			return 0;
		}

		default: return 0;
	}

	return 1;
}

static void handle_char(prompt_t *prompt, const char c)
{
	size_t i;

	get_cursor_pos(&prompt->cursor_x, &prompt->cursor_y);
	write(STDERR_FILENO, &c, sizeof(c));
	buffer_insert(&prompt->buffer, prompt->curpos, &c, sizeof(c));
	i = ++prompt->curpos;
	write(STDERR_FILENO, prompt->buffer.data + i, prompt->buffer.length - i);
	if(prompt->cursor_x + 1 >= (int) prompt->win_width)
		print_newline();
	else
		set_cursor_pos(prompt->cursor_x + 1, prompt->cursor_y);
}

static void prompt_read(prompt_t *prompt)
{
	char buff;

	while(read(STDIN_FILENO, &buff, sizeof(buff)) > 0)
	{
		if(!isprint(buff))
		{
			if(handle_ctrl(prompt, buff))
				return;
			continue;
		}
		else
			handle_char(prompt, buff);
	}
}

// TODO Auto completion
// TODO Vi mode
// TODO History
const char *prompt(const char *prompt_var)
{
	static prompt_t *prompt = NULL;
	prompt_buffer_t tmp;
	const char *cmd;

	set_tmodes();
	if(!prompt && !(prompt = mem_alloc(sizeof(prompt_t))))
		return NULL;
	tmp = prompt->buffer;
	bzero(prompt, sizeof(prompt_t));
	prompt->prompt_var = prompt_var;
	prompt->buffer = tmp;
	prompt_prepare(prompt);

	prompt_read(prompt);

	restore_tmodes();
	if(prompt->discard)
	{
		buffer_free(&prompt->buffer);
		return NULL;
	}
	cmd = buffer_dump(&prompt->buffer);
	buffer_free(&prompt->buffer);
	return cmd;
}
