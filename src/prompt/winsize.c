#include "prompt.h"

void get_winsize(size_t *win_width, size_t *win_height)
{
	struct winsize ws;

	*win_width = 0;
	*win_height = 0;
	if(ioctl(STDIN_FILENO, TIOCGWINSZ, &ws) < 0)
		return;
	*win_width = ws.ws_col;
	*win_height = ws.ws_row;
}
