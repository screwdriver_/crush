#include "prompt.h"

#define CURSOR_POS	"\033[6n"
#define BUFF_SIZE	64

static int find_response(const char *buff)
{
	size_t i = 0, j;

	while(i < BUFF_SIZE - 6)
	{
		if(buff[i] == 033 && buff[i + 1] == '[' && isdigit(buff[i + 2]))
		{
			j = 3;
			while(i + j < BUFF_SIZE && isdigit(buff[i + j]))
				++j;
			if(i + j + 1 < BUFF_SIZE
				&& buff[i + j] == ';' && isdigit(buff[i + j + 1]))
			{
				j += 2;
				while(i + j < BUFF_SIZE && isdigit(buff[i + j]))
					++j;
				if(buff[i + j] == 'R')
					return i;
			}
		}
		++i;
	}
	return -1;
}

// TODO Safe read and write
void get_cursor_pos(int *cursor_x, int *cursor_y)
{
	char buff[BUFF_SIZE];
	int i = -1;

	*cursor_x = -1;
	*cursor_y = -1;
	while(i < 0)
	{
		write(STDOUT_FILENO, CURSOR_POS, strlen(CURSOR_POS));
		bzero(buff, sizeof(buff));
		if(read(STDIN_FILENO, buff, sizeof(buff)) < 0)
			fatal_error("An error happened with terminal!");
		i = find_response(buff);
	}
	i += 2;
	// TODO Check if ANSI code is cursor position
	*cursor_y = atoi(buff + i) - 1;
	while((unsigned) i < sizeof(buff) - 1 && isdigit(buff[i]))
		++i;
	++i;
	*cursor_x = atoi(buff + i) - 1;
}

void set_cursor_pos(const int cursor_x, const int cursor_y)
{
	static char *str = NULL;

	if(!str)
		str = termcap_load("cm");
	tputs(tgoto(str, cursor_x, cursor_y), 1, term_write);
}

void move_cursor_up(void)
{
	// TODO
}

void move_cursor_down(void)
{
	// TODO
}

void move_cursor_left(void)
{
	static char *str = NULL;

	if(!str)
		str = termcap_load("le");
	tputs(str, 1, term_write);
}

void move_cursor_right(void)
{
	static char *str = NULL;

	if(!str)
		str = termcap_load("nd");
	tputs(str, 1, term_write);
}
