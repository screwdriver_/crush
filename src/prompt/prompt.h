#ifndef PROMPT_H
# define PROMPT_H

# include "../crush.h"
# include <curses.h>
# include <sys/ioctl.h>
# include <term.h>
# include <termcap.h>

# define PROMPT_BUFFER_SIZE			64
# define BUFFER_DEFAULT_CAPACITY	256

typedef struct
{
	size_t length, capacity;
	char *data;
} prompt_buffer_t;

typedef struct
{
	const char *prompt_var;

	size_t win_width, win_height;
	int cursor_x, cursor_y;
	int cursor_base_x, cursor_base_y;

	size_t curpos;
	prompt_buffer_t buffer;

	char discard;
} prompt_t;

void get_cursor_pos(int *cursor_x, int *cursor_y);
void set_cursor_pos(int cursor_x, int cursor_y);
void move_cursor_up(void);
void move_cursor_down(void);
void move_cursor_left(void);
void move_cursor_right(void);

void get_winsize(size_t *win_width, size_t *win_height);

char *termcap_load(char *name);
int term_write(int c);

void buffer_insert(prompt_buffer_t *buffer, size_t pos,
	const char *buff, size_t len);
void buffer_erase(prompt_buffer_t *buffer, size_t pos, size_t len);
const char *buffer_dump(prompt_buffer_t *buffer);
void buffer_free(prompt_buffer_t *buffer);

void init_prompt(void);
void print_prompt(const char *prompt_var);
const char *prompt(const char *prompt_var);

#endif
