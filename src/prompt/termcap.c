#include "prompt.h"

char *termcap_load(char *name)
{
	char *val;

	if(!(val = tgetstr(name, NULL)))
		fatal_error("Failed to load termcap!");
	return val;
}

int term_write(int c)
{
	return write(STDOUT_FILENO, &c, 1);
}
