#include "prompt.h"
#include "readline.h"

// TODO Free structs at end
static buffer_t **get_buffers(const int fd)
{
	static readline_fd_t *fds = NULL;
	readline_fd_t *f, *prev = NULL;

	f = fds;
	while(f && f->fd != fd)
	{
		prev = f;
		f = f->next;
	}
	if(!f)
	{
		if(!(f = mem_alloc(sizeof(readline_fd_t))))
			return NULL;
		f->fd = fd;
	}
	if(prev && prev->next)
		prev->next = prev->next->next;
	f->next = fds;
	fds = f;
	return &f->first_buffer;
}

static void push_buff(buffer_t **buffers, char *buffer, const size_t len)
{
	buffer_t *buff, *tmp;

	if(!(buff = mem_alloc(sizeof(buffer_t))))
		return;
	buff->len = len;
	memcpy(buff->buff, buffer, len);
	if((tmp = *buffers))
	{
		while(tmp && tmp->next)
			tmp = tmp->next;
		tmp->next = buff;
	}
	else
		*buffers = buff;
}

static size_t line_length(buffer_t **buffers)
{
	buffer_t *b;
	size_t n = 0;
	char *s;

	b = *buffers;
	while(b)
	{
		if((s = memchr(b->buff + b->i, '\n', b->len - b->i)))
		{
			n += s - (b->buff + b->i);
			break;
		}
		else
			n += b->len - b->i;
		b = b->next;
	}
	return n;
}

static const char *get_line(buffer_t **buffers)
{
	size_t length;
	char *buff;
	buffer_t *b, *next, *prev = NULL;
	size_t i = 0, len;
	char *s;

	if((length = line_length(buffers)) == 0 || !(buff = mem_alloc(length + 1)))
		return NULL;
	b = *buffers;
	while(b)
	{
		next = b->next;
		if((s = memchr(b->buff + b->i, '\n', b->len - b->i)))
			len = s - (b->buff + b->i);
		else
			len = b->len - b->i;
		memcpy(buff + i, b->buff + b->i, len);
		i += len;
		if(s)
			b->i += len + 1;
		else
			b->i += len;
		if(b->i >= b->len)
		{
			if(prev)
				prev->next = next;
			else
				*buffers = next;
			free((void *) b);
			b = next;
		}
		else
		{
			if(s)
				break;
			prev = b;
			b = next;
		}
	}
	return buff;
}

const char *read_line(const int fd)
{
	buffer_t **buffers;
	size_t l;
	char buffer[PROMPT_BUFFER_SIZE];
	char *nl;

	if(!(buffers = get_buffers(fd)))
		return NULL;
	while((l = read(fd, buffer, sizeof(buffer))) > 0)
	{
		push_buff(buffers, buffer, l);
		if((nl = memchr(buffer, '\n', l)))
			break;
	}
	return get_line(buffers);
}
