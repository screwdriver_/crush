#include "prompt.h"

void buffer_insert(prompt_buffer_t *buffer, const size_t pos,
	const char *buff, const size_t len)
{
	size_t new_len, new_capacity;

	if(!buffer || pos > buffer->length)
		return;
	new_len = buffer->length + len;
	if(new_len >= buffer->capacity)
	{
		if(buffer->capacity == 0)
			new_capacity = BUFFER_DEFAULT_CAPACITY;
		else
			new_capacity = buffer->capacity + (buffer->capacity / 2);
		if(!(buffer->data = realloc(buffer->data, new_capacity)))
			return;
		buffer->capacity = new_capacity;
	}
	memmove(buffer->data + pos + len, buffer->data + pos,
		buffer->length - pos);
	memcpy(buffer->data + pos, buff, len);
	buffer->length = new_len;
}

void buffer_erase(prompt_buffer_t *buffer, const size_t pos, const size_t len)
{
	if(!buffer || pos > buffer->length || len == 0)
		return;
	memmove(buffer->data + pos, buffer->data + pos + len,
		buffer->length - (pos + len));
	buffer->length -= len;
}

const char *buffer_dump(prompt_buffer_t *buffer)
{
	char *str;

	if(!(str = mem_alloc(buffer->length + 2)))
		return NULL;
	if(buffer->length > 0)
		memcpy(str, buffer->data, buffer->length + 1);
	str[buffer->length] = '\n';
	return str;
}

void buffer_free(prompt_buffer_t *buffer)
{
	if(!buffer)
		return;
	free((void *) buffer->data);
	bzero(buffer, sizeof(prompt_buffer_t));
}
