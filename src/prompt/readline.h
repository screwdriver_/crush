#ifndef READLINE_H
# define READLINE_H

# include "prompt.h"

# define BUFF_SIZE	64

typedef struct buffer
{
	struct buffer *next;

	size_t i, len;
	char buff[BUFF_SIZE];
} buffer_t;

typedef struct readline_fd
{
	struct readline_fd *next;

	int fd;
	buffer_t *first_buffer;
} readline_fd_t;

const char *read_line(int fd);

#endif
