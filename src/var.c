#include "crush.h"

static void var_push(var_t **vars, var_t *var)
{
	var_t *tmp;

	if(!(tmp = *vars))
	{
		*vars = var;
		return;
	}
	while(tmp->next)
		tmp = tmp->next;
	tmp->next = var;
}

static void free_struct(var_t *v)
{
	if(!v)
		return;
	free((void *)v->name);
	free((void *)v->value);
	free((void *)v);
}

static var_t *var_new(const char *name, const char *value)
{
	var_t *v;

	if(!name || !value)
		return NULL;
	if(!(v = mem_alloc(sizeof(var_t))))
		return NULL;
	if(!(v->name = c_strdup(name)) || !(v->value = c_strdup(value)))
	{
		free_struct(v);
		return NULL;
	}
	return v;
}

void env_load(var_t **vars, char **environ)
{
	var_t *v = NULL;
	size_t len;
	char *s;
	size_t name_len;

	var_freeall(*vars);
	*vars = NULL;
	while(*environ)
	{
		len = strlen(*environ);
		if((s = strchr(*environ, '=')))
			name_len = s - *environ;
		else
			name_len = len;

		if(!(v = mem_alloc(sizeof(var_t)))
			|| !(v->name = strndup(*environ, name_len)))
			goto fail;
		if(s && s[1] && !(v->value = c_strdup(s + 1)))
			goto fail;
		v->env = 1;

		var_push(vars, v);
		++environ;
	}
	return;

fail:
	if(v)
		free_struct(v);
	var_freeall(*vars);
	*vars = NULL;
}

const char *var_serialize(var_t *var)
{
	size_t l0, l1;
	char *buff;

	if(!var)
		return NULL;
	l0 = strlen(var->name);
	l1 = strlen(var->value);
	if(!(buff = mem_alloc(l0 + l1 + 2)))
		return NULL;
	strcpy(buff, var->name);
	buff[l0] = '=';
	strcpy(buff + l0 + 1, var->value);
	return buff;
}

static size_t count_env(var_t *v)
{
	size_t n = 0;

	while(v)
	{
		if(v->env)
			++n;
		v = v->next;
	}
	return n;
}

const char **env_array(var_t *vars)
{
	size_t i;
	const char **arr;
	var_t *v;

	i = count_env(vars);
	if(!(arr = mem_alloc(sizeof(char *) * (i + 1))))
		return NULL;
	i = 0;
	v = vars;
	while(v)
	{
		if(v->env)
			arr[i++] = var_serialize(v);
		v = v->next;
	}
	return arr;
}

var_t *var_get(var_t *vars, const char *name)
{
	var_t *tmp;

	if(!name)
		return NULL;
	tmp = vars;
	while(tmp)
	{
		if(strcmp(tmp->name, name) == 0)
			return tmp;
		tmp = tmp->next;
	}
	return NULL;
}

var_t *var_set(var_t **vars, const char *name, const char *value)
{
	var_t *v;

	if((v = var_get(*vars, name)))
	{
		free((void *) v->value);
		v->value = c_strdup(value);
	}
	else if((v = var_new(name, value)))
	{
		v->next = *vars;
		*vars = v;
	}
	return v;
}

void var_assign(var_t **vars, const char *str)
{
	const char *name, *s;

	if(!(s = strchr(str, '=')) || s == str)
		return;
	if(!(name = c_strndup(str, s - str)))
		return;
	var_set(vars, name, s + 1);
	free((void *) name);
}

void var_export(var_t *vars, const char *name)
{
	var_t *var;

	if((var = var_get(vars, name)))
		var->env = 1;
}

void var_unset(var_t **vars, const char *name)
{
	var_t *v, *prev = NULL, *tmp;

	v = *vars;
	while(v)
	{
		if(strcmp(v->name, name) == 0)
		{
			tmp = v->next;
			free_struct(v);
			if(prev)
				prev->next = tmp;
			else
				*vars = tmp;
			break;
		}
		prev = v;
		v = v->next;
	}
}

// TODO All vars
void var_set_defaults(var_t **vars)
{
	char buff[PATH_MAX + 1];
	var_t *shlvl_var;
	char *shlvl;

	var_set(vars, "PS1", "$ ");
	var_set(vars, "PS2", "> ");
	var_set(vars, "PS4", "+ ");

	bzero(buff, sizeof(buff));
	getcwd(buff, PATH_MAX);
	var_set(vars, "PWD", buff);

	if((shlvl_var = var_get(*vars, "SHLVL"))
		&& (shlvl = itoa(atoi(shlvl_var->value) + 1)))
	{
		var_set(vars, "SHLVL", shlvl);
		free((void *) shlvl);
	}
	else
		var_set(vars, "SHLVL", "1");

	if(!(var_get(*vars, "TERM")))
		var_set(vars, "TERM", DEFAULT_TERM);

	// TODO `_`?
}

var_t *var_dup(var_t *v)
{
	var_t *dup = NULL, *tmp, *prev;

	if(!v)
		return NULL;
	while(v)
	{
		if(!(tmp = var_new(v->name, v->value)))
		{
			var_freeall(dup);
			return NULL;
		}
		tmp->env = v->env;
		if(dup)
		{
			prev->next = tmp;
			prev = prev->next;
		}
		else
			prev = dup = tmp;
		v = v->next;
	}
	return dup;
}

void var_freeall(var_t *vars)
{
	var_t *next;

	if(!vars)
		return;
	while(vars)
	{
		next = vars->next;
		free_struct(vars);
		vars = next;
	}
}

void var_print(const var_t *var)
{
	if(!var)
		return;
	// TODO functions
	print(STDOUT_FILENO, var->name);
	printchar(STDOUT_FILENO, '=');
	print_quoted(STDOUT_FILENO, var->value);
	printchar(STDOUT_FILENO, '\n');
}

void var_print_all(const var_t *vars)
{
	while(vars)
	{
		var_print(vars);
		vars = vars->next;
	}
}
