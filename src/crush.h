#ifndef CRUSH_H
# define CRUSH_H

# include <ctype.h>
# include <errno.h>
# include <fcntl.h>
# include <signal.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <termios.h>
# include <unistd.h>

# ifdef __linux__
#  include <linux/limits.h>
# elif __APPLE__
#  include <sys/syslimits.h>
# endif

# define DEFAULT_TERM	"dumb"

# define MIN(n0, n1)	((n0) < (n1) ? (n0) : (n1))

typedef struct var
{
	struct var *next;

	const char *name;
	const char *value;
	char env, readonly;
} var_t;

typedef struct alias
{
	struct alias *next;

	const char *name;
	const char *value;
} alias_t;

typedef struct token token_t;

extern pid_t shell_pgid;
extern int shell_terminal;
extern int shell_is_interactive;

extern int exit_code;

extern var_t *vars;
extern alias_t *aliases;

void print(int fd, const char *message);
void printchar(int fd, const char c);
void printnbr(int fd, int nbr);
void println(int fd, const char *message);
void print_quoted(const int fd, const char *str);
void *mem_alloc(size_t size);
char *c_strdup(const char *s);
char *c_strndup(const char *s, size_t n);
int c_open(const char *pathname, int flags);
int c_open_(const char *pathname, int flags, mode_t mode);
pid_t c_fork(void);
int c_pipe(int pipe_fds[2]);
int c_read(int fd, void *buff, size_t count);
int c_write(int fd, const void *buff, size_t count);
void reset_color(int fd);
int is_nbr(const char *str);
char *itoa(int i);
char *ltoa(long i);
const char *merge_path(const char *p0, const char *p1);

const char *new_tmp_file(void);
char *read_file(const char *file);
char *read_file_fd(int fd);

char *get_brace_end(const char *str);

int exec_command(const char *cmd);
void exec_source(const char *file);
const char *subshell_exec_command(const char *cmd, int *exit_code);

void enable_signals(void);
void disable_signals(void);

int is_name_char(const char c, const int first);
int is_name(const char *str);

void env_load(var_t **vars, char **environ);
const char *var_serialize(var_t *var);
const char **env_array(var_t *vars);
var_t *var_get(var_t *vars, const char *name);
var_t *var_set(var_t **vars, const char *name, const char *value);
void var_assign(var_t **vars, const char *str);
void var_export(var_t *vars, const char *name);
void var_unset(var_t **vars, const char *name);
void var_set_defaults(var_t **vars);
var_t *var_dup(var_t *vars);
void var_freeall(var_t *vars);
void var_print(const var_t *var);
void var_print_all(const var_t *vars);

alias_t *alias_get(const char *name);
alias_t *alias_set(const char *name, const char *value);
void alias_unset(const alias_t *alias);
void alias_unset_all(void);
void alias_print(const alias_t *alias);
void alias_print_all(void);

void errno_error(const char *str);
void parse_error(token_t *token);
void ueof(void);
void command_not_found(const char *message);
void crash(const char *message);
void builtin_error(const char *builtin, const char *message);
__attribute__((noreturn))
void fatal_error(const char *message);

int check_close(const char *str);
const char *get_command(void);

#endif
