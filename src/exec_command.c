#include "crush.h"
#include "prompt/readline.h"
#include "lexer/lexer.h"
#include "parser/parser.h"
#include "expansions/expansions.h"
#include "exec/exec.h"

int exit_code = 0;

int exec_command(const char *cmd)
{
	token_t *tokens = NULL;
	ast_t *ast = NULL;
	exec_context_t context;

	bzero(&context, sizeof(exec_context_t));
	exit_code = 0;
	if(!(tokens = lexer(cmd)))
	{
		exit_code = 127;
		goto free;
	}
	// TODO print_tokens(tokens); // TODO Remove
	if(!(ast = parse(tokens)))
	{
		exit_code = 127;
		goto free;
	}
	// TODO print_ast(ast, 0); // TODO Remove
	perform_expansions(&tokens);
	exec(ast, &context);

free:
	free_tree(ast);
	free_tokens(tokens);
	return exit_code;
}

void exec_source(const char *file)
{
	int fd;
	const char *buff;

	if((fd = open(file, O_RDONLY)) < 0)
		return ;
	while((buff = read_line(fd)))
	{
		exec_command(buff);
		free((void *) buff);
	}
	close(fd);
}
