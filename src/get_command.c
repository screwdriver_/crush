#include "crush.h"
#include "prompt/prompt.h"

static void join(const char **input, const char *str)
{
	size_t l0, l1;
	char *buff;

	if(!input || !str)
		return;
	l0 = (*input ? strlen(*input) : 0);
	l1 = strlen(str);
	if(!(buff = mem_alloc(l0 + l1 + 1)))
		goto fail;
	if(*input)
		memcpy(buff, *input, l0);
	memcpy(buff + l0, str, l1);
	free((void *) *input);
	*input = buff;
	return;

fail:
	free((void *) buff);
	*input = NULL;
}

const char *get_command(void)
{
	const char *input = NULL, *str = NULL;

	if(!(input = prompt("PS1")))
		return NULL;
	while(!check_close(input))
	{
		if(!(str = prompt("PS2")))
			goto fail;
		join(&input, str);
		free((void *) str);
		if(!input)
			goto fail;
	}
	return input;

fail:
	free((void *) input);
	free((void *) str);
	return NULL;
}
