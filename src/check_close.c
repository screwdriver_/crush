#include "crush.h"

// TODO Handle keywords

static int handle_backslash(const char **str);
static int handle_simple_quote(const char **str);
static int handle_double_quote(const char **str);
static int handle_brace(const char **str);
static int handle_curly_brace(const char **str);
static int handle_backquote(const char **str);

static int handle_backslash(const char **str)
{
	if((*str)[0] != '\\')
		return 1;
	if(!(*str)[1])
		return 0;
	*str += 2;
	return 1;
}

static int handle_simple_quote(const char **str)
{
	if(**str != '\'')
		return 1;
	++(*str);
	while(**str && **str != '\'')
		++(*str);
	if(!**str)
		return 0;
	++(*str);
	return 1;
}

static int handle_double_quote(const char **str)
{
	int close = 1;

	if(**str != '"')
		return 1;
	++(*str);
	while(**str && close && **str != '"')
	{
		if(**str == '\\')
			close = handle_backslash(str);
		else if(**str == '\'')
			close = handle_double_quote(str);
		else if((*str)[0] == '$' && (*str)[1] == '(')
		{
			++(*str);
			close = handle_brace(str);
		}
		else if((*str)[0] == '$' && (*str)[1] == '{')
		{
			++(*str);
			close = handle_curly_brace(str);
		}
		else if(**str == '`')
			close = handle_backquote(str);
		else
			++(*str);
	}
	if(!**str)
		return 0;
	++(*str);
	return close;
}

static int handle_brace(const char **str)
{
	int close = 1;

	if(**str != '(')
		return 1;
	++(*str);
	while(**str && close && **str != ')')
	{
		if(**str == '\\')
			close = handle_backslash(str);
		else if(**str == '\'')
			close = handle_simple_quote(str);
		else if(**str == '\'')
			close = handle_double_quote(str);
		else if(**str == '(')
			close = handle_brace(str);
		else if(**str == '{')
			close = handle_curly_brace(str);
		else if(**str == '`')
			close = handle_backquote(str);
		else
			++(*str);
	}
	if(!**str)
		return 0;
	++(*str);
	return close;
}

static int handle_curly_brace(const char **str)
{
	int close = 1;

	if(**str != '{')
		return 1;
	++(*str);
	while(**str && close && **str != '}')
	{
		if(**str == '\\')
			close = handle_backslash(str);
		else if(**str == '\'')
			close = handle_simple_quote(str);
		else if(**str == '\'')
			close = handle_double_quote(str);
		else if(**str == '(')
			close = handle_brace(str);
		else if(**str == '{')
			close = handle_curly_brace(str);
		else if(**str == '`')
			close = handle_backquote(str);
		else
			++(*str);
	}
	if(!**str)
		return 0;
	++(*str);
	return close;
}

static int handle_backquote(const char **str)
{
	int close = 1;

	if(**str != '`')
		return 1;
	++(*str);
	while(**str && close && **str != '`')
	{
		if(**str == '\\')
			close = handle_backslash(str);
		else if(**str == '\'')
			close = handle_simple_quote(str);
		else if(**str == '\'')
			close = handle_double_quote(str);
		else if(**str == '(')
			close = handle_brace(str);
		else if(**str == '{')
			close = handle_curly_brace(str);
		else
			++(*str);
	}
	if(!**str)
		return 0;
	++(*str);
	return close;
}

int check_close(const char *str)
{
	int close = 1;

	if(!str)
		return 0;
	while(*str && close)
	{
		if(*str == '\\')
			close = handle_backslash(&str);
		else if(*str == '\'')
			close = handle_simple_quote(&str);
		else if(*str == '\'')
			close = handle_double_quote(&str);
		else if(*str == '(')
			close = handle_brace(&str);
		else if(*str == '{')
			close = handle_curly_brace(&str);
		else if(*str == '`')
			close = handle_backquote(&str);
		else
			++str;
	}
	return close;
}
