#include "lexer.h"

int is_blank(char c)
{
	return (c == ' ' || c == '\t');
}

int is_operator_char(char c)
{
	return (strchr("()<>&|;", c) != NULL);
}

int is_quote(char c)
{
	return (c == '\'' || c == '"');
}

int is_brace_open(char c)
{
	return (c == '(' || c == '{' || c == '[');
}

int is_expansion_begin(const char *str)
{
	if(!str)
		return 0;
	if(str[0] == '`')
		return 1;
	if(str[0] == '$' && is_brace_open(str[1]))
		return 1;
	return 0;
}
