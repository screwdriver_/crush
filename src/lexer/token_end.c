#include "lexer.h"

static token_pair_t token_types[] = {
	{AND_IF_T, "&&"},
	{OR_IF_T, "||"},
	{DSEMI_T, ";;"},
	{SEMI_T, ";"},
	{AMPR_T, "&"},
	{PIPE_T, "|"},
	{DLESS_T, "<<"},
	{DGREAT_T, ">>"},
	{LESSAND_T, "<&"},
	{GREATAND_T, ">&"},
	{LESSGREAT_T, "<>"},
	{DLESSDASH_T, "<<-"},
	{CLOBBER_T, ">|"},
	{LESS_T, "<"},
	{GREAT_T, ">"},
	{LPAREN_T, "("},
	{RPAREN_T, ")"}
};

static const token_pair_t *get_special(const char *input)
{
	size_t i, len;

	for(i = 0; i < sizeof(token_types) / sizeof(token_pair_t); ++i)
	{
		len = strlen(token_types[i].str);
		if(strncmp(input, token_types[i].str, len) == 0)
			return token_types + i;
	}

	return NULL;
}

token_type_t get_token_end(const char **input)
{
	const token_pair_t *pair;

	if(**input == '\n')
	{
		++(*input);
		return NEWLINE_T;
	}
	if((pair = get_special(*input)))
	{
		*input += strlen(pair->str);
		return pair->type;
	}
	if(is_operator_char(**input))
	{
		++(*input);
		return WORD_T;
	}

	while(**input && !is_operator_char(**input)
		&& !is_blank(**input) && **input != '\n')
	{
		if((*input)[0] == '\\' && (*input)[1])
			(*input) += 2;
		else if(is_quote(**input))
			skip_quote(input);
		else if(is_expansion_begin(*input))
			skip_expansion(input);
		else
			++(*input);
	}
	// TODO IO_NUMBER
	// TODO NAME

	return WORD_T;
}
