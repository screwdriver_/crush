#include "lexer.h"

token_t *alloc_token(const char *begin, const char *end)
{
	token_t *tok;
	size_t length;

	if(end <= begin)
		return NULL;
	length = (size_t) (end - begin);
	if(!(tok = mem_alloc(sizeof(token_t)))
		|| !(tok->data = strndup(begin, length)))
	{
		free((void *) tok);
		return NULL;
	}
	tok->type = WORD_T;
	return tok;
}

void delimit_token(token_t **tokens, const char *begin, const char *end,
	token_type_t type)
{
	token_t *tok, *t;

	if(!(tok = alloc_token(begin, end)))
		return;
	tok->type = type;
	if(!*tokens)
		*tokens = tok;
	else
	{
		t = *tokens;
		while(t->next)
			t = t->next;
		t->next = tok;
		tok->prev = t;
	}
}

#ifdef DEBUG
void print_tokens(token_t *tokens)
{
	while(tokens)
	{
		printf("- %i: %s\n", tokens->type, tokens->data);
		tokens = tokens->next;
	}
}
#endif

void free_token(token_t *token)
{
	if(!token)
		return;
	free((void *) token->data);
	free((void *) token);
}

void free_tokens(token_t *tokens)
{
	token_t *next;

	while(tokens)
	{
		next = tokens->next;
		free_token(tokens);
		tokens = next;
	}
}
