#include "lexer.h"

static void skip_comment(const char **str)
{
	if(**str == '#')
		++(*str);
	while(**str && **str != '\n')
		++(*str);
}

void skip_blanks(const char **str)
{
	while(is_blank(**str) || **str == '#')
	{
		while(is_blank(**str))
			++(*str);
		if(**str == '#')
			skip_comment(str);
	}
}

void skip_quote(const char **str)
{
	char quote;

	quote = *((*str)++);
	while(**str && **str != quote)
	{
		if(quote == '"')
		{
			if((*str)[0] == '\\' && (*str)[1])
				(*str) += 2;
			else if(is_expansion_begin(*str))
				skip_expansion(str);
			else
				++(*str);
		}
		else
			++(*str);
	}
	if(**str == quote)
		++(*str);
}

static char get_expansion_close(char open)
{
	switch(open)
	{
		case '`': return '`';
		case '(': return ')';
		case '{': return '}';
		case '[': return ']';
	}
	return 0;
}

void skip_braces(const char **str)
{
	char close;

	close = get_expansion_close(*((*str)++));
	while(**str && **str != close)
	{
		if((*str)[0] == '\\' && (*str)[1])
			*str += 2;
		else if(is_quote(**str))
			skip_quote(str);
		else if(is_brace_open(**str))
			skip_braces(str);
		else if(is_expansion_begin(*str))
			skip_expansion(str);
		else
			++(*str);
	}
	if(**str == close)
		++(*str);
}

// TODO Handle specific case of ${...}
void skip_expansion(const char **str)
{
	char close;

	if(**str == '$')
		++(*str);
	close = get_expansion_close(*((*str)++));
	while(**str && **str != close)
	{
		if((*str)[0] == '\\' && (*str)[1])
			*str += 2;
		else if(is_quote(**str))
			skip_quote(str);
		else if(is_brace_open(**str))
			skip_braces(str);
		else if(is_expansion_begin(*str))
			skip_expansion(str);
		else
			++(*str);
	}
	if(**str == close)
		++(*str);
}
