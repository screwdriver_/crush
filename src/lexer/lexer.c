#include "lexer.h"

static token_pair_t keywords[] = {
	{IF_T, "if"},
	{THEN_T, "then"},
	{ELSE_T, "else"},
	{ELIF_T, "elif"},
	{FI_T, "fi"},
	{DO_T, "do"},
	{DONE_T, "done"},
	{CASE_T, "case"},
	{ESAC_T, "esac"},
	{WHILE_T, "while"},
	{UNTIL_T, "until"},
	{FOR_T, "for"},
	{LBRACE_T, "{"},
	{RBRACE_T, "}"},
	{BANG_T, "!"},
	{IN_T, "in"}
};

static token_type_t assign_seps[] = {
	AND_IF_T, OR_IF_T, SEMI_T, AMPR_T, DSEMI_T, PIPE_T,
	DO_T, UNTIL_T,
	LBRACE_T, RBRACE_T, BANG_T
};

static token_type_t get_keyword(const char *str)
{
	size_t i = 0;

	while(i < sizeof(keywords) / sizeof(token_pair_t))
	{
		if(strcmp(str, keywords[i].str) == 0)
			return keywords[i].type;
		++i;
	}
	return WORD_T;
}

static void set_keywords(token_t *t)
{
	while(t)
	{
		if(t->type == WORD_T)
			t->type = get_keyword(t->data);
		t = t->next;
	}
}

static int is_assign_sep(const token_type_t type)
{
	size_t i = 0;

	while(i < sizeof(assign_seps) / sizeof(token_type_t))
	{
		if(type == assign_seps[i])
			return 1;
		++i;
	}
	return 0;
}

static int is_assign_word(const char *str)
{
	if(!str || !strchr(str, '=') || str[0] == '=' || isdigit(str[0]))
		return 0;
	while(*str != '=')
	{
		if(*str != '_' && !isalnum(*str))
			return 0;
		++str;
	}
	return 1;
}

static void set_assignment_words(token_t *t)
{
	while(t)
	{
		while(t && is_assign_sep(t->type))
			t = t->next;
		while(t && t->type == WORD_T && is_assign_word(t->data))
		{
			t->type = ASSIGNMENT_WORD_T;
			t = t->next;
		}
		while(t && !is_assign_sep(t->type))
			t = t->next;
	}
}

token_t *lexer(const char *input)
{
	token_t *tokens = NULL;
	const char *begin;
	token_type_t type;

	if(!input)
		return NULL;
	skip_blanks(&input);
	begin = input;
	while(*input)
	{
		type = get_token_end(&input);
		delimit_token(&tokens, begin, input, type);
		skip_blanks(&input);
		begin = input;
	}
	set_keywords(tokens);
	set_assignment_words(tokens);
	return tokens;
}
