#ifndef LEXER_H
# define LEXER_H

# include "../crush.h"

typedef enum
{
	WORD_T = 0,
	ASSIGNMENT_WORD_T = 1,
	NAME_T = 2,
	NEWLINE_T = 3,
	IO_NUMBER_T = 4,
	AND_IF_T = 5,
	OR_IF_T = 6,
	SEMI_T = 7,
	AMPR_T = 8,
	DSEMI_T = 9,
	PIPE_T = 10,
	LESS_T = 11,
	GREAT_T = 12,
	DLESS_T = 13,
	DGREAT_T = 14,
	LESSAND_T = 15,
	GREATAND_T = 16,
	LESSGREAT_T = 17,
	DLESSDASH_T = 18,
	CLOBBER_T = 19,
	IF_T = 20,
	THEN_T = 21,
	ELSE_T = 22,
	ELIF_T = 23,
	FI_T = 24,
	DO_T = 25,
	DONE_T = 26,
	CASE_T = 27,
	ESAC_T = 28,
	WHILE_T = 29,
	UNTIL_T = 30,
	FOR_T = 31,
	LPAREN_T = 32,
	RPAREN_T = 33,
	LBRACE_T = 34,
	RBRACE_T = 35,
	BANG_T = 36,
	IN_T = 37
} token_type_t;

typedef struct token
{
	struct token *next, *prev;

	token_type_t type;
	char *data;
} token_t;

typedef struct
{
	token_type_t type;
	const char *str;
} token_pair_t;

int is_blank(char c);
int is_operator_char(char c);
int is_quote(char c);
int is_brace_open(char c);
int is_expansion_begin(const char *c);

void skip_blanks(const char **str);
void skip_quote(const char **str);
void skip_braces(const char **str);
void skip_expansion(const char **str);

token_t *alloc_token(const char *begin, const char *end);
void delimit_token(token_t **tokens, const char *begin, const char *end,
	token_type_t type);
# ifdef DEBUG
void print_tokens(token_t *tokens);
# endif
void free_token(token_t *token);
void free_tokens(token_t *tokens);

token_t *lexer(const char *input);
token_type_t get_token_end(const char **input);

#endif
