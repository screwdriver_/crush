#include "crush.h"

alias_t *aliases = NULL;

alias_t *alias_get(const char *name)
{
	alias_t *a;

	if(!name)
		return NULL;
	a = aliases;
	while(a)
	{
		if(strcmp(a->name, name) == 0)
			return a;
		a = a->next;
	}
	return NULL;
}

alias_t *alias_set(const char *name, const char *value)
{
	alias_t *a;

	if(!name || !value)
		return NULL;
	if(!(a = mem_alloc(sizeof(alias_t))))
		return NULL;
	if(!(a->name = strdup(name)) || !(a->value = strdup(value)))
	{
		free((void *) a->name);
		free((void *) a);
		return NULL;
	}
	a->next = aliases;
	aliases = a;
	return a;
}

void alias_unset(const alias_t *alias)
{
	alias_t *prev;

	if(!alias)
		return;
	if(alias != aliases)
	{
		prev = aliases;
		while(prev && prev->next && prev->next != alias)
			prev = prev->next;
		if(prev)
			prev->next = alias->next;
	}
	else
		aliases = aliases->next;
	free((void *) alias->name);
	free((void *) alias->value);
	free((void *) alias);
}

void alias_unset_all(void)
{
	alias_t *a, *next;

	a = aliases;
	while(a)
	{
		next = a->next;
		free((void *) a->name);
		free((void *) a->value);
		free((void *) a);
		a = next;
	}
	aliases = NULL;
}

void alias_print(const alias_t *alias)
{
	if(!alias)
		return;
	print(STDOUT_FILENO, alias->name);
	printchar(STDOUT_FILENO, '=');
	print_quoted(STDOUT_FILENO, alias->value);
	printchar(STDOUT_FILENO, '\n');
}

void alias_print_all(void)
{
	alias_t *a;

	a = aliases;
	while(a)
	{
		alias_print(a);
		a = a->next;
	}
}
