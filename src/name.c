#include "crush.h"

int is_name_char(const char c, const int first)
{
	return ((first ? isalpha(c) : isalnum(c)) || c == '_');
}

int is_name(const char *str)
{
	int first;

	first = 1;
	while(*str)
	{
		if(!is_name_char(*str, first))
			return 0;
		++str;
		first = 0;
	}
	return 1;
}
