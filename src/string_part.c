#include "string_part.h"

string_part_t *string_part_new(const char *begin, const size_t length)
{
	string_part_t *p;

	if(!begin || length == 0)
		return NULL;
	if(!(p = mem_alloc(sizeof(string_part_t))))
		return NULL;
	if(!(p->str = c_strndup(begin, length)))
	{
		free((void *) p);
		return NULL;
	}
	return p;
}

void string_part_push(string_part_t **parts, string_part_t *p)
{
	string_part_t *tmp;

	if(!parts || !p)
		return;
	if((tmp = *parts))
	{
		while(tmp->next)
			tmp = tmp->next;
		tmp->next = p;
	}
	else
		*parts = p;
}

static size_t string_length(string_part_t *p)
{
	size_t n = 0;

	while(p)
	{
		n += strlen(p->str);
		p = p->next;
	}
	return n;
}

char *string_part_merge(string_part_t *parts)
{
	size_t len;
	char *buff;
	string_part_t *p;
	size_t i = 0, l;

	if(!parts)
		return NULL;
	len = string_length(parts);
	if(!(buff = mem_alloc(len + 1)))
		return NULL;
	p = parts;
	while(p)
	{
		l = strlen(p->str);
		memcpy(buff + i, p->str, l);
		i += l;
		p = p->next;
	}
	return buff;
}

void string_part_free(string_part_t *part)
{
	if(!part)
		return;
	free((void *) part->str);
	free((void *) part);
}

void string_part_free_all(string_part_t *parts)
{
	string_part_t *p, *next;

	if(!parts)
		return;
	p = parts;
	while(p)
	{
		next = p->next;
		string_part_free(p);
		p = next;
	}
}
