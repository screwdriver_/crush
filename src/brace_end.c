#include "crush.h"
#include "lexer/lexer.h"

static char get_end_char(const char c)
{
	switch(c)
	{
		case '(': return ')';
		case '{': return '}';
		case '`': return '`';
		default: return 0;
	}
}

char *get_brace_end(const char *str)
{
	char begin, end;
	const char *s;

	if(!str)
		return NULL;
	end = get_end_char(begin = *(str++));
	while(*str && *str != end)
	{
		if(*str == '(' || *str == '{' || *str == '`')
		{
			if(*(s = get_brace_end(str)))
				str = s + 1;
			else
				str = s;
		}
		else if(str[0] == '\\' && str[1])
			str += 2;
		else if(*str == '\'' || *str == '"')
			skip_quote(&str);
		else
			++str;
	}
	return (char *) str;
}
