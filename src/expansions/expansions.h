#ifndef EXPANSIONS_H
# define EXPANSIONS_H

# include "../crush.h"
# include "../lexer/lexer.h"

# define IS_LOGICAL_OR(op)	((op) == LOGICAL_OR_O)
# define IS_LOGICAL_AND(op)	((op) == LOGICAL_AND_O)
# define IS_BITWISE_OR(op)	((op) == BITWISE_OR_O)
# define IS_BITWISE_XOR(op)	((op) == BITWISE_XOR_O)
# define IS_BITWISE_AND(op)	((op) == BITWISE_AND_O)
# define IS_EQUALITY(op)	((op) == EQUAL_O || (op) == NOT_EQUAL_O)
# define IS_COMPARISON(op)	((op) == LOWER_O || (op) == LOWER_OR_EQUAL_O\
	|| (op) == HIGHER_O || (op) == HIGHER_OR_EQUAL_O)
# define IS_SHIFT(op)		((op) == LEFT_SHIFT_O || (op) == RIGHT_SHIFT_O)
# define IS_SUM(op)			((op) == ADD_O || (op) == SUBTRACT_O)
# define IS_FACTOR(op)		((op) == MULTIPLY_O || (op) == DIVIDE_O\
	|| (op) == MODULO_O)

// TODO All C operators
enum exp_operator
{
	// TODO Logical and bitwise NOT
	ADD_O = 0,
	SUBTRACT_O = 1,
	MULTIPLY_O = 2,
	DIVIDE_O = 3,
	MODULO_O = 4,
	LEFT_SHIFT_O = 5,
	RIGHT_SHIFT_O = 6,
	LOWER_O = 7,
	LOWER_OR_EQUAL_O = 8,
	HIGHER_O = 9,
	HIGHER_OR_EQUAL_O = 10,
	EQUAL_O = 11,
	NOT_EQUAL_O = 12,
	BITWISE_AND_O = 13,
	BITWISE_XOR_O = 14,
	BITWISE_OR_O = 15,
	LOGICAL_AND_O = 16,
	LOGICAL_OR_O = 17,
	// TODO Ternary
	// TODO Assignements

	UNKNOWN_O
};

int perform_expansions(token_t **t);

char *command_substitution(char *s, size_t length);
char *arithmetic_expansion(char *s, size_t length);

void split_fields(token_t **head, token_t **t, const char *ifs);
void remove_quotes(token_t *t);

#endif
