#include "expansions.h"

static const char *operators[] = {
	[ADD_O] = "+",
	[SUBTRACT_O] = "-",
	[MULTIPLY_O] = "*",
	[DIVIDE_O] = "/",
	[MODULO_O] = "%",
	[LEFT_SHIFT_O] = "<<",
	[RIGHT_SHIFT_O] = ">>",
	[LOWER_O] = "<",
	[LOWER_OR_EQUAL_O] = "<=",
	[HIGHER_O] = ">",
	[HIGHER_OR_EQUAL_O] = ">=",
	[EQUAL_O] = "==",
	[NOT_EQUAL_O] = "!=",
	[BITWISE_AND_O] = "&",
	[BITWISE_XOR_O] = "^",
	[BITWISE_OR_O] = "|",
	[LOGICAL_AND_O] = "&&",
	[LOGICAL_OR_O] = "||"
};

// TODO Handle all C operators

static void skip_spaces(const char **s)
{
	while(**s && isspace(**s))
		++(*s);
}

static enum exp_operator parse_operator(const char *s)
{
	size_t i;

	for(i = 0; i < sizeof(operators) / sizeof(*operators); ++i)
	{
		if(strncmp(s, operators[i], strlen(operators[i])) == 0)
			return i;
	}
	return UNKNOWN_O;
}

static void eat_operator(const char **s, const enum exp_operator op)
{
	if(op == UNKNOWN_O)
		return;
	*s += strlen(operators[op]);
}

static long eval(const char **s);

static long parse_nbr(const char **s)
{
	long n = 0;
	int neg;

	skip_spaces(s);
	if((neg = **s == '-') || **s == '+')
		++(*s);
	while(isdigit(**s))
	{
		n *= 10;
		n += **s - '0';
		++(*s);
	}
	return (neg ? -n : n);
}

static long parse_operand(const char **s)
{
	long val;

	skip_spaces(s);
	if(**s == '(')
	{
		++(*s);
		val = eval(s);
		skip_spaces(s);
		if(**s == ')')
			++(*s);
		return val;
	}
	// TODO Handle variables
	return parse_nbr(s);
}

static long parse_logical_or(const char **s)
{
	long n0;

	n0 = parse_operand(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_LOGICAL_OR(parse_operator(*s)))
			return n0;
		eat_operator(s, LOGICAL_OR_O);
		n0 |= parse_operand(s);
	}
	return n0;
}

static long parse_logical_and(const char **s)
{
	long n0;

	n0 = parse_logical_or(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_LOGICAL_AND(parse_operator(*s)))
			return n0;
		eat_operator(s, LOGICAL_AND_O);
		n0 &= parse_logical_or(s);
	}
	return n0;
}

static long parse_bitwise_or(const char **s)
{
	long n0;

	n0 = parse_logical_and(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_BITWISE_OR(parse_operator(*s)))
			return n0;
		eat_operator(s, BITWISE_OR_O);
		n0 |= parse_logical_and(s);
	}
	return n0;
}

static long parse_bitwise_xor(const char **s)
{
	long n0;

	n0 = parse_bitwise_or(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_BITWISE_XOR(parse_operator(*s)))
			return n0;
		eat_operator(s, BITWISE_XOR_O);
		n0 ^= parse_bitwise_or(s);
	}
	return n0;
}

static long parse_bitwise_and(const char **s)
{
	long n0;

	n0 = parse_bitwise_xor(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_BITWISE_AND(parse_operator(*s)))
			return n0;
		eat_operator(s, BITWISE_AND_O);
		n0 &= parse_bitwise_xor(s);
	}
	return n0;
}

static long parse_equality(const char **s)
{
	long n0, n1;
	enum exp_operator op;

	n0 = parse_bitwise_and(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_EQUALITY(op = parse_operator(*s)))
			return n0;
		eat_operator(s, op);
		n1 = parse_bitwise_and(s);
		if(op == EQUAL_O)
			n0 = (n0 == n1);
		else if(op == NOT_EQUAL_O)
			n0 = (n0 != n1);
	}
	return n0;
}

static long parse_comparions(const char **s)
{
	long n0, n1;
	enum exp_operator op;

	n0 = parse_equality(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_COMPARISON(op = parse_operator(*s)))
			return n0;
		eat_operator(s, op);
		n1 = parse_equality(s);
		if(op == LOWER_O)
			n0 = (n0 < n1);
		else if(op == LOWER_OR_EQUAL_O)
			n0 = (n0 <= n1);
		else if(op == HIGHER_O)
			n0 = (n0 > n1);
		else if(op == HIGHER_OR_EQUAL_O)
			n0 = (n0 >= n1);
	}
	return n0;
}

static long parse_shifts(const char **s)
{
	long n0, n1;
	enum exp_operator op;

	n0 = parse_comparions(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_SHIFT(op = parse_operator(*s)))
			return n0;
		eat_operator(s, op);
		n1 = parse_comparions(s);
		if(op == LEFT_SHIFT_O)
			n0 <<= n1;
		else if(op == RIGHT_SHIFT_O)
			n0 >>= n1;
	}
	return n0;
}

static long parse_sums(const char **s)
{
	long n0, n1;
	enum exp_operator op;

	n0 = parse_shifts(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_SUM(op = parse_operator(*s)))
			return n0;
		eat_operator(s, op);
		n1 = parse_shifts(s);
		if(op == ADD_O)
			n0 += n1;
		else if(op == SUBTRACT_O)
			n0 -= n1;
	}
	return n0;
}

static long parse_factors(const char **s)
{
	long n0, n1;
	enum exp_operator op;

	n0 = parse_sums(s);
	while(**s)
	{
		skip_spaces(s);
		if(!IS_FACTOR(op = parse_operator(*s)))
			return n0;
		eat_operator(s, op);
		n1 = parse_sums(s);
		if(op == MULTIPLY_O)
			n0 *= n1;
		else if(op == DIVIDE_O)
			n0 /= n1;
		else if(op == MODULO_O)
			n0 %= n1;
	}
	return n0;
}

static long eval(const char **s)
{
	return parse_factors(s);
}

// TODO Expansions inside arithmetic expansions
char *arithmetic_expansion(char *s, const size_t length)
{
	char *begin;
	long r;

	if(!s || length == 0)
		return NULL;
	begin = s;
	r = eval((const char **) &s);
	if(s < begin + length)
	{
		printf("parse error at \"%.*s\"\n", (int) (length - (s - begin)), s); // TODO remove
		// TODO error
		return NULL;
	}
	return ltoa(r);
}
