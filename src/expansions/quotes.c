#include "expansions.h"

static void handle_token(token_t *t)
{
	char *data;
	size_t i = 0;
	const char *d;

	if(!t->data)
		return;
	if(!(data = mem_alloc(strlen(t->data) + 1)))
		return;
	d = t->data;
	while(*d)
	{
		if(*d == '\"')
			++d;
		else if(*d == '\'')
		{
			++d;
			while(*d && *d != '\'')
				data[i++] = *(d++);
			if(*d)
				++d;
		}
		else if(*d == '\\')
		{
			++d;
			if(*d)
				data[i++] = *(d++);
		}
		else
			data[i++] = *(d++);
	}
	free((void *) t->data);
	t->data = data;
}

void remove_quotes(token_t *t)
{
	while(t)
	{
		handle_token(t);
		t = t->next;
	}
}
