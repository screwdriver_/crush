#include "expansions.h"

static inline int is_space(const char c, const char *ifs)
{
	return (strchr(ifs, c) != NULL);
}

static void skip_spaces(const char **s, const char *ifs)
{
	while(**s && is_space(**s, ifs))
		++(*s);
}

static size_t field_length(const char *s, const char *ifs)
{
	size_t n = 0;
	const char *tmp;

	while(s[n] && !is_space(s[n], ifs))
	{
		if(s[n] == '\'' || s[n] == '"')
		{
			tmp = s + n;
			skip_quote(&tmp);
			n = tmp - s;
		}
		else
			++n;
	}
	return n;
}

static void insert_field(const char *s, const size_t length,
	token_t **t, const int first)
{
	char *str;
	token_t *tok;

	if(!(str = c_strndup(s, length)))
		goto fail;
	if(!first)
	{
		if(!(tok = mem_alloc(sizeof(token_t))))
			goto fail;
		tok->data = str;
		if((tok->next = (*t)->next))
			tok->next->prev = tok;
		if((tok->prev = *t))
			tok->prev->next = tok;
		*t = tok;
	}
	else
		(*t)->data = str;
	return;

fail:
	free((void *) str);
	// TODO Stop fields splitting, abort command
}

void split_fields(token_t **head, token_t **t, const char *ifs)
{
	const char *buff, *s;
	int first = 1;
	size_t len;

	if(!head || !t || !*t || !ifs || strlen(ifs) == 0)
		return;
	buff = s = (*t)->data;
	while(*s)
	{
		skip_spaces(&s, ifs);
		if(!*s)
			break;
		len = field_length(s, ifs);
		insert_field(s, len, t, first);
		first = 0;
		s += len;
	}
	free((void *) buff);
	if(first)
		(*t)->data = NULL;
}
