#include "expansions.h"
#include "../string_part.h"

static int is_special_parameter_char(const char c)
{
	return (isdigit(c) || strchr("@*#?-$!", c));
}

static char *next_expansion(char *str)
{
	// TODO tilde expansion
	// TODO check if escaped
	while(*str)
	{
		if(str[0] == '$' && (is_special_parameter_char(str[1])
			|| is_name_char(str[1], 1) || str[1] == '{' || str[1] == '('))
			return str;
		++str;
	}
	return str;
}

static char *expansion_end(char *s)
{
	char *end = NULL;

	if(s[0] == '$')
	{
		if(s[1] == '(')
			end = get_brace_end(s + 1);
		else if (s[1] == '{')
			end = get_brace_end(s + 1);
		if(!end || *end)
			return end + 1;
		else
		{
			// TODO Error
		}
	}
	// TODO
	return NULL;
}

static char *expand(char *s, const size_t length)
{
	if(s[0] == '$')
	{
		if(s[1] == '(' && s[2] == '(')
			return arithmetic_expansion(s + 3, length - 5);
		else if(s[1] == '(')
			return command_substitution(s + 2, length - 3);
		else if(s[1] == '{')
		{
			// TODO
			return NULL;
		}
	}
	return NULL;
}

static void token_perform_expansions(token_t **head, token_t *t)
{
	string_part_t *parts = NULL;
	char *begin, *end;
	char *buff;

	// TODO
	(void) head;
	begin = t->data;
	while(*begin)
	{
		end = next_expansion(begin);
		if(end != begin)
			string_part_push(&parts, string_part_new(begin, end - begin));
		if(!*end)
			break;
		begin = end;
		end = expansion_end(begin);
		if(end != begin)
		{
			if(!(buff = expand(begin, end - begin)))
				goto fail;
			string_part_push(&parts, string_part_new(buff, strlen(buff)));
			free((void *) buff);
		}
		begin = end;
	}
	if(!(buff = string_part_merge(parts)))
		goto fail;
	free((void *) t->data);
	t->data = buff;

fail:
	string_part_free_all(parts);
}

static int handle_token(token_t **head, token_t **t, const char *ifs)
{
	token_perform_expansions(head, *t);
	split_fields(head, t, ifs);
	return 0; // TODO
}

int perform_expansions(token_t **t)
{
	var_t *ifs_var;
	const char *ifs;
	token_t *tok;

	if((ifs_var = var_get(vars, "IFS")))
		ifs = ifs_var->value;
	else
		ifs = " \t\n";
	tok = *t;
	while(tok)
	{
		if(handle_token(t, &tok, ifs) < 0)
			return -1;
		tok = tok->next;
	}
	// TODO pathname expansions
	remove_quotes(*t);
	return 0;
}
