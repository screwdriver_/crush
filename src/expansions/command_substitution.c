#include "expansions.h"

static void exec_(const char *tmp_file, char *s, const size_t length)
{
	int fd;

	// TODO Enable signals?
	if((fd = c_open_(tmp_file, O_CREAT | O_WRONLY, 0600)) < 0)
		exit(127); // TODO Error?
	dup2(fd, STDOUT_FILENO);
	s[length] = '\0';
	exec_command(s);
	exit(exit_code);
}

// TODO If `, remove \ in front of every ` inside of the expansion
char *command_substitution(char *s, const size_t length)
{
	pid_t pid;
	const char *tmp_file = NULL;
	int status;
	char *buff = NULL;

	if(!s || length == 0)
		return NULL;
	if(!(tmp_file = new_tmp_file()) || (pid = c_fork()) < 0)
	{
		// TODO error?
		goto end;
	}
	if(pid)
	{
		waitpid(pid, &status, 0);
		buff = read_file(tmp_file);
		goto end;
	}
	exec_(tmp_file, s, length);

end:
	free((void *) tmp_file);
	return buff;
}
