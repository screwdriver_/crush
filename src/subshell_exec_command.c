#include "crush.h"

const char *subshell_exec_command(const char *cmd, int *exit_code)
{
	const char *file = NULL;
	pid_t pid;
	int fd;
	int status;
	const char *content = NULL;

	if(!cmd || !exit_code
		|| !(file = new_tmp_file()) || (pid = c_fork()) < 0)
	{
		free((void *) file);
		return NULL;
	}
	if(pid)
	{
		status = waitpid(pid, &status, 0);
		if(WIFEXITED(status))
			*exit_code = WEXITSTATUS(status);
		else if(WIFSIGNALED(status))
			*exit_code = 128 + WTERMSIG(status);
		content = read_file(file);
		remove(file);
		free((void *) file);
		return content;
	}
	if((fd = open(file, O_WRONLY | O_CREAT, 0644)) < 0
		|| dup2(fd, STDOUT_FILENO) < 0)
		exit(1); // TODO Error message?
	exit(exec_command(cmd));
}
