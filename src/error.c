#include "crush.h"
#include "lexer/lexer.h"
#include "parser/parser.h"

void errno_error(const char *str)
{
	if(!errno)
		return;
	print(STDERR_FILENO, "crush: ");
	if(str)
	{
		print(STDERR_FILENO, str);
		print(STDERR_FILENO, ": ");
	}
	println(STDERR_FILENO, strerror(errno));
}

void parse_error(token_t *token)
{
	if(parse_err)
		return;
	parse_err = 1;
	if(!token)
	{
		ueof();
		return;
	}
	print(STDERR_FILENO, "crush: parse error near token `");
	if(strcmp(token->data, "\n") == 0)
		print(STDERR_FILENO, "newline");
	else
		print(STDERR_FILENO, token->data);
	print(STDERR_FILENO, "`\n");
}

void ueof(void)
{
	println(STDERR_FILENO, "crush: unexpected end-of-file!");
}

void command_not_found(const char *cmd)
{
	print(STDERR_FILENO, "crush: ");
	if(cmd)
		print(STDERR_FILENO, cmd);
	print(STDERR_FILENO, ": ");
	println(STDERR_FILENO, "command not found");
}

void crash(const char *message)
{
	print(STDERR_FILENO, "crush: ");
	println(STDERR_FILENO, message);
}

void builtin_error(const char *builtin, const char *message)
{
	print(STDERR_FILENO, "crush: ");
	print(STDERR_FILENO, builtin);
	print(STDERR_FILENO, ": ");
	println(STDERR_FILENO, message);
}

__attribute__((noreturn))
void fatal_error(const char *message)
{
	print(STDERR_FILENO, "Fatal error: ");
	print(STDERR_FILENO, message);
	print(STDERR_FILENO, " (errno: ");
	print(STDERR_FILENO, strerror(errno));
	println(STDERR_FILENO, ")");
	exit(1);
}
