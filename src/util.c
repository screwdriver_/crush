#include "crush.h"

void print(const int fd, const char *message)
{
	if(!message)
		return;
	write(fd, message, strlen(message));
}

void printchar(const int fd, const char c)
{
	write(fd, &c, sizeof(c));
}

void printnbr(const int fd, const int nbr)
{
	if(nbr < 0)
		print(fd, "-");
	if(nbr > 9 || nbr < -9)
		printnbr(fd, abs(nbr / 10));
	printchar(fd, '0' + abs(nbr % 10));
}

void println(const int fd, const char *message)
{
	print(fd, message);
	write(fd, "\n", 1);
}

void print_quoted(const int fd, const char *str)
{
	printchar(fd, '\'');
	while(*str)
	{
		if(*str == '\'')
			print(fd, "'\"'\"'");
		else
			printchar(fd, *str);
		++str;
	}
	printchar(fd, '\'');
}

void *mem_alloc(size_t size)
{
	void *ptr;

	errno = 0;
	if(!(ptr = malloc(size)))
	{
		errno_error("malloc");
		return NULL;
	}
	bzero(ptr, size);
	return ptr;
}

char *c_strdup(const char *s)
{
	char *ptr;

	if(!(ptr = strdup(s)))
		errno_error("malloc");
	return ptr;
}

char *c_strndup(const char *s, size_t n)
{
	char *ptr;

	if(!(ptr = strndup(s, n)))
		errno_error("malloc");
	return ptr;
}

int c_open(const char *pathname, int flags)
{
	int r;

	if((r = open(pathname, flags)) < 0)
		errno_error(pathname);
	return r;
}

int c_open_(const char *pathname, const int flags, const mode_t mode)
{
	int r;

	if((r = open(pathname, flags, mode)) < 0)
		errno_error(pathname);
	return r;
}

pid_t c_fork(void)
{
	pid_t pid;

	errno = 0;
	if((pid = fork()) < 0 && errno)
		errno_error("fork");
	return pid;
}

int c_pipe(int pipe_fds[2])
{
	int r;

	if((r = pipe(pipe_fds)) < 0)
		errno_error("pipe");
	return r;
}

int c_read(int fd, void *buff, size_t count)
{
	size_t i = 0;
	ssize_t len;

	while(i < count && (len = read(fd, buff + i, count - i)) > 0)
		i += len;
	return (len < 0 ? -1 : 0);
}

int c_write(int fd, const void *buff, size_t count)
{
	size_t i = 0;
	ssize_t len;

	while(i < count && (len = write(fd, buff + i, count - i)) >= 0)
		i += len;
	return (len < 0 ? -1 : 0);
}

void reset_color(int fd)
{
	const char *str = "\033[0m";
	write(fd, str, strlen(str));
}

int is_nbr(const char *str)
{
	if(!str || !*str)
		return 0;
	while(*str)
		if(!isdigit(*(str++)))
			return 0;
	return 1;
}

static size_t intlen(long i)
{
	size_t len = 0;

	if(i == 0)
		return 1;
	if(i < 0)
		++len;
	while(i != 0)
	{
		i /= 10;
		++len;
	}
	return len;
}

char *itoa(int i)
{
	char *buff;
	size_t n;

	if(!(buff = mem_alloc((n = intlen(i)) + 1)))
		return NULL;
	if(i < 0)
	{
		buff[0] = '-';
		while(n-- > 1)
		{
			buff[n] = '0' + abs(i % 10);
			i /= 10;
		}
	}
	else
		while(n-- > 0)
		{
			buff[n] = '0' + abs(i % 10);
			i /= 10;
		}
	return buff;
}

char *ltoa(long i)
{
	char *buff;
	size_t n;

	if(!(buff = mem_alloc((n = intlen(i)) + 1)))
		return NULL;
	if(i < 0)
	{
		buff[0] = '-';
		while(n-- > 1)
		{
			buff[n] = '0' + abs((int) (i % 10));
			i /= 10;
		}
	}
	else
		while(n-- > 0)
		{
			buff[n] = '0' + abs((int) (i % 10));
			i /= 10;
		}
	return buff;
}

const char *merge_path(const char *p0, const char *p1)
{
	size_t l0, l1;
	int add_slash;
	char *buff;

	if(!p0 || !p1)
		return NULL;
	if(!(l0 = strlen(p0)) || !(l1 = strlen(p1)))
		return NULL;
	if(!(add_slash = (p0[l0 - 1] != '/' && p1[0] != '/')))
		buff = mem_alloc(l0 + l1 + 1);
	else
		buff = mem_alloc(l0 + l1 + 2);
	if(!buff)
		return NULL;
	strcpy(buff, p0);
	if(add_slash)
	{
		buff[l0] = '/';
		strcpy(buff + l0 + 1, p1);
	}
	else
		strcpy(buff + l0, p1);
	return buff;
}
