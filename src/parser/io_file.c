#include "parser.h"

static token_type_t redirect_types[] = {
	LESS_T, GREAT_T, LESSAND_T, GREATAND_T, DGREAT_T, LESSGREAT_T, CLOBBER_T
};

static int is_redirect_type(const token_type_t type)
{
	size_t i = 0;

	while(i < sizeof(redirect_types) / sizeof(token_type_t))
	{
		if(type == redirect_types[i])
			return 1;
		++i;
	}
	return 0;
}

ast_t *p_io_file(token_t **tokens)
{
	ast_t *ast;
	token_t *tok;

	tok = *tokens;
	if(!tok)
		return NULL;
	if(!(ast = alloc_node(IO_FILE_A, tok)))
		goto fail;
	if(!is_redirect_type(tok->type))
		goto fail;
	NEXT_TOKEN(tok);
	if(!tok || tok->type != WORD_T)
		goto error;
	NEXT_TOKEN(tok);
	*tokens = tok;
	return ast;

error:
	parse_error(tok);

fail:
	free_tree(ast);
	return NULL;
}
