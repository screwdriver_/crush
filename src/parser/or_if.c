#include "parser.h"

ast_t *p_or_if(token_t **tokens)
{
	ast_t *node;

	if(!*tokens)
		return NULL;
	if((*tokens)->type != OR_IF_T)
		return NULL;
	if(!(node = alloc_node(OR_IF_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
