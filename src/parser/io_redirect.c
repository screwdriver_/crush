#include "parser.h"

ast_t *p_io_redirect(token_t **tokens)
{
	ast_t *ast, *child;

	if(!*tokens)
		return NULL;
	if(!(ast = alloc_node(IO_REDIRECT_A, *tokens)))
		goto fail;
	if((*tokens)->type == IO_NUMBER_T)
		NEXT_TOKEN(*tokens);
	if(!(child = p_io_file(tokens)) && !(child = p_io_here(tokens)))
		goto fail;
	add_child(ast, child);

	return ast;

fail:
	free_tree(ast);
	return NULL;
}
