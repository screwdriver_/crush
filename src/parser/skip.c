#include "parser.h"

void skip_newlines(token_t **tokens)
{
	while(*tokens && (*tokens)->type == NEWLINE_T)
		NEXT_TOKEN(*tokens);
}
