#include "parser.h"

ast_t *p_word(token_t **tokens)
{
	ast_t *node;
	if(!*tokens)
		return NULL;
	if((*tokens)->type != WORD_T)
		return NULL;
	if(!(node = alloc_node(WORD_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
