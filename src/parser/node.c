#include "parser.h"

#ifdef DEBUG
typedef struct
{
	node_type_t type;
	const char *str;
} node_type_pair_t;

static node_type_pair_t types[] = {
	{PROGRAM_A, "PROGRAM"},
	{COMPLETE_COMMAND_A, "COMPLETE_COMMAND"},
	{SEPARATOR_OP_A, "SEPARATOR_OP"},
	{AND_OR_A, "AND_OR"},
	{AND_IF_A, "AND_IF"},
	{OR_IF_A, "OR_IF"},
	{PIPELINE_A, "PIPELINE"},
	{BANG_A, "BANG"},
	{PIPE_A, "PIPE"},
	{SIMPLE_COMMAND_A, "SIMPLE_COMMAND"},
	{ASSIGNMENT_WORD_A, "ASSIGNMENT_WORD"},
	{WORD_A, "WORD"},
	{REDIRECT_LIST_A, "REDIRECT_LIST"},
	{FUNCTION_DEFINITION_A, "FUNCTION_DEFINITION"},
	{BRACE_GROUP_A, "BRACE_GROUP"},
	{SUBSHELL_A, "SUBSHELL"},
	{FOR_CLAUSE_A, "FOR_CLAUSE"},
	{CASE_CLAUSE_A, "CASE_CLAUSE"},
	{IF_CLAUSE_A, "IF_CLAUSE"},
	{WHILE_CLAUSE_A, "WHILE_CLAUSE"},
	{UNTIL_CLAUSE_A, "UNTIL_CLAUSE"},
	{COMPOUND_LIST_A, "COMPOUND_LIST"},
	{NAME_A, "NAME"},
	{DO_GROUP_A, "DO_GROUP"},
	{SEQUENTIAL_SEP_A, "SEQUENTIAL_SEP"},
	{IN_A, "IN"},
	{WORDLIST_A, "WORDLIST"},
	{CASE_LIST_NS_A, "CASE_LIST_NS"},
	{CASE_LIST_A, "CASE_LIST"},
	{CASE_ITEM_NS_A, "CASE_ITEM_NS"},
	{CASE_ITEM_A, "CASE_ITEM"},
	{PATTERN_A, "PATTERN"},
	{ELSE_PART_A, "ELSE_PART"},
	{FUNCTION_BODY_A, "FUNCTION_BODY"},
	{IO_REDIRECT_A, "IO_REDIRECT"},
	{IO_FILE_A, "IO_FILE"},
	{IO_HERE_A, "IO_HERE"}
};
#endif

ast_t *alloc_node(node_type_t type, token_t *first_token)
{
	ast_t *ast;

	if(!first_token)
	{
		ueof();
		return NULL;
	}
	if((ast = mem_alloc(sizeof(ast_t))))
	{
		ast->type = type;
		ast->first_token = first_token;
	}
	return ast;
}

void add_child(ast_t *parent, ast_t *child)
{
	ast_t *n;

	if(!parent || !child)
		return;
	if(parent->children)
	{
		n = parent->children;
		while(n->next)
			n = n->next;
		n->next = child;
	}
	else
		parent->children = child;
}

#ifdef DEBUG
void print_node(ast_t *ast, size_t level)
{
	size_t i;

	for(i = 0; i < level; ++i)
		printf("\t");
	printf("- %s\n", types[ast->type].str);
	// TODO Print tokens
}

void print_ast(ast_t *ast, size_t level)
{
	ast_t *n;

	if(!ast)
	{
		printf("--- EMPTY ---\n");
		return;
	}
	print_node(ast, level);
	n = ast->children;
	while(n)
	{
		print_ast(n, level + 1);
		n = n->next;
	}
}
#endif

void free_tree(ast_t *ast)
{
	ast_t *tmp, *next;

	if(!ast)
		return;
	tmp = ast->children;
	while(tmp)
	{
		next = tmp->next;
		free_tree((void *) tmp);
		tmp = next;
	}
	free((void *) ast);
}
