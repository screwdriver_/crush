#include "parser.h"

ast_t *p_and_if(token_t **tokens)
{
	ast_t *node;

	if(!*tokens)
		return NULL;
	if((*tokens)->type != AND_IF_T)
		return NULL;
	if(!(node = alloc_node(AND_IF_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
