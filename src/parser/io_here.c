#include "parser.h"

ast_t *p_io_here(token_t **tokens)
{
	ast_t *ast;
	token_t *tok;

	tok = *tokens;
	if(!tok)
		return NULL;
	if(!(ast = alloc_node(IO_HERE_A, tok)))
		goto fail;
	if(tok->type != DLESS_T && tok->type != DLESSDASH_T)
		goto fail;
	NEXT_TOKEN(tok);
	if(!tok || tok->type != WORD_T)
		goto error;
	NEXT_TOKEN(tok);
	*tokens = tok;
	return ast;

error:
	parse_error(tok);

fail:
	free_tree(ast);
	return NULL;
}
