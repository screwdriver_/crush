#include "parser.h"

ast_t *p_pipe(token_t **tokens)
{
	ast_t *node;

	if(!*tokens)
		return NULL;
	if((*tokens)->type != PIPE_T)
		return NULL;
	if(!(node = alloc_node(PIPE_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
