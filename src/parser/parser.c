#include "parser.h"

int parse_err;

ast_t *parse(token_t *tokens)
{
	ast_t *ast, *child;

	parse_err = 0;
	if(!(ast = alloc_node(PROGRAM_A, tokens)))
		return NULL;
	skip_newlines(&tokens);
	while(tokens)
	{
		if(!(child = p_complete_command(&tokens)))
			goto fail;
		add_child(ast, child);
		skip_newlines(&tokens);
	}
	if(parse_err)
		goto fail;
	return ast;

fail:
	free_tree(ast);
	return NULL;
}
