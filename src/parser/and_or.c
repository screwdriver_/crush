#include "parser.h"

ast_t *p_and_or(token_t **tokens)
{
	ast_t *ast, *child;

	if(!(ast = alloc_node(AND_OR_A, *tokens)))
		goto fail;
	if(!(child = p_pipeline(tokens)))
		goto fail;
	add_child(ast, child);
	while(*tokens)
	{
		if(!(child = p_and_if(tokens)) && !(child = p_or_if(tokens)))
			break;
		add_child(ast, child);
		skip_newlines(tokens);
		if(!(child = p_pipeline(tokens)))
			goto fail;
		add_child(ast, child);
	}
	if(parse_err)
		goto fail;
	return ast;

fail:
	free_tree(ast);
	return NULL;
}
