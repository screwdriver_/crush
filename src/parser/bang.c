#include "parser.h"

ast_t *p_bang(token_t **tokens)
{
	ast_t *node;

	if(!*tokens)
		return NULL;
	if((*tokens)->type != BANG_T)
		return NULL;
	if(!(node = alloc_node(BANG_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
