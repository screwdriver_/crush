#include "parser.h"

ast_t *p_complete_command(token_t **tokens)
{
	ast_t *ast, *child;

	if(!(ast = alloc_node(COMPLETE_COMMAND_A, *tokens)))
		goto fail;
	while(*tokens && (*tokens)->type != NEWLINE_T)
	{
		if(!(child = p_and_or(tokens)))
			goto fail;
		add_child(ast, child);
		if(!(child = p_separator_op(tokens)))
			break;
		add_child(ast, child);
	}
	if(parse_err)
		goto fail;
	return ast;

fail:
	free_tree(ast);
	return NULL;
}
