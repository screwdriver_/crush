#include "parser.h"

ast_t *p_simple_command(token_t **tokens)
{
	ast_t *ast, *child;

	if(!(ast = alloc_node(SIMPLE_COMMAND_A, *tokens)))
		goto fail;
	while(*tokens)
	{
		if(!(child = p_assignment_word(tokens))
			&& !(child = p_io_redirect(tokens)))
			break;
		add_child(ast, child);
	}
	while(*tokens && (*tokens)->type == WORD_T)
	{
		if(!(child = p_word(tokens)))
			break;
		add_child(ast, child);
	}
	while(*tokens)
	{
		if(!(child = p_word(tokens))
			&& !(child = p_io_redirect(tokens)))
			break;
		add_child(ast, child);
	}
	return ast;

fail:
	free_tree(ast);
	return NULL;
}
