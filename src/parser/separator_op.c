#include "parser.h"

ast_t *p_separator_op(token_t **tokens)
{
	ast_t *node;

	if(!*tokens)
		return NULL;
	if((*tokens)->type != SEMI_T && (*tokens)->type != AMPR_T)
		return NULL;
	if(!(node = alloc_node(SEPARATOR_OP_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
