#ifndef PARSER_H
# define PARSER_H

# include "../lexer/lexer.h"

# define NEXT_TOKEN(t)	((t) = (t)->next)

// TODO Remove WORD_A and ASSIGNMENT_WORD_A? (because of expansions adding tokens)
typedef enum
{
	PROGRAM_A = 0,
	COMPLETE_COMMAND_A,
	SEPARATOR_OP_A,
	AND_OR_A,
	AND_IF_A,
	OR_IF_A,
	PIPELINE_A,
	BANG_A,
	PIPE_A,
	SIMPLE_COMMAND_A,
	ASSIGNMENT_WORD_A,
	WORD_A,
	REDIRECT_LIST_A,
	FUNCTION_DEFINITION_A,
	BRACE_GROUP_A,
	SUBSHELL_A,
	FOR_CLAUSE_A,
	CASE_CLAUSE_A,
	IF_CLAUSE_A,
	WHILE_CLAUSE_A,
	UNTIL_CLAUSE_A,
	COMPOUND_LIST_A,
	NAME_A,
	DO_GROUP_A,
	SEQUENTIAL_SEP_A,
	IN_A,
	WORDLIST_A,
	CASE_LIST_NS_A,
	CASE_LIST_A,
	CASE_ITEM_NS_A,
	CASE_ITEM_A,
	PATTERN_A,
	ELSE_PART_A,
	FUNCTION_BODY_A,
	IO_REDIRECT_A,
	IO_FILE_A,
	IO_HERE_A
} node_type_t;

typedef struct ast
{
	node_type_t type;
	token_t *first_token;

	struct ast *next;
	struct ast *children;
} ast_t;

extern int parse_err;

ast_t *alloc_node(node_type_t type, token_t *first_token);
void add_child(ast_t *parent, ast_t *child);
# ifdef DEBUG
void print_node(ast_t *ast, size_t level);
void print_ast(ast_t *ast, size_t level);
# endif
void free_tree(ast_t *ast);

void skip_newlines(token_t **tokens);

void unexpected_token(token_t *token);

ast_t *parse(token_t *tokens);
ast_t *p_complete_command(token_t **tokens);
ast_t *p_separator_op(token_t **tokens);
ast_t *p_and_or(token_t **tokens);
ast_t *p_and_if(token_t **tokens);
ast_t *p_or_if(token_t **tokens);
ast_t *p_pipeline(token_t **tokens);
ast_t *p_bang(token_t **tokens);
ast_t *p_pipe(token_t **tokens);
ast_t *p_command(token_t **tokens);
ast_t *p_simple_command(token_t **tokens);
ast_t *p_assignment_word(token_t **tokens);
ast_t *p_word(token_t **tokens);
// TODO
ast_t *p_io_redirect(token_t **tokens);
ast_t *p_io_file(token_t **tokens);
ast_t *p_io_here(token_t **tokens);

#endif
