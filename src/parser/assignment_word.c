#include "parser.h"

ast_t *p_assignment_word(token_t **tokens)
{
	ast_t *node;

	if(!*tokens)
		return NULL;
	if((*tokens)->type != ASSIGNMENT_WORD_T)
		return NULL;
	if(!(node = alloc_node(ASSIGNMENT_WORD_A, *tokens)))
		return NULL;
	NEXT_TOKEN(*tokens);
	return node;
}
