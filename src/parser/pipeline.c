#include "parser.h"

ast_t *p_pipeline(token_t **tokens)
{
	ast_t *ast, *child;

	if(!(ast = alloc_node(PIPELINE_A, *tokens)))
		goto fail;
	if((child = p_bang(tokens)))
		add_child(ast, child);
	if(!(child = p_command(tokens)))
		goto fail;
	add_child(ast, child);
	while(*tokens)
	{
		if(!(child = p_pipe(tokens)))
			break;
		add_child(ast, child);
		skip_newlines(tokens);
		if(!(child = p_command(tokens)))
			goto fail;
		add_child(ast, child);
	}
	return ast;

fail:
	free_tree(ast);
	return NULL;
}
