#ifndef STRING_PART_T
# define STRING_PART_T

# include "crush.h"

typedef struct string_part
{
	struct string_part *next;
	const char *str;
} string_part_t;

string_part_t *string_part_new(const char *begin, size_t length);
void string_part_push(string_part_t **parts, string_part_t *p);
char *string_part_merge(string_part_t *parts);
void string_part_free(string_part_t *part);
void string_part_free_all(string_part_t *parts);

#endif
